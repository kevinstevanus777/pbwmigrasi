<?php

session_start();

if (!isset($_SESSION["login"])) {

    header("Location: login.php");
}


require "functions.php";
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Pesona Wisata</title>

    <!-- ================= Favicon ================== -->
    <!-- Standard -->
    <link rel="shortcut icon" href="http://placehold.it/64.png/000/fff">
    <!-- Retina iPad Touch Icon-->
    <link rel="apple-touch-icon" sizes="144x144" href="http://placehold.it/144.png/000/fff">
    <!-- Retina iPhone Touch Icon-->
    <link rel="apple-touch-icon" sizes="114x114" href="http://placehold.it/114.png/000/fff">
    <!-- Standard iPad Touch Icon-->
    <link rel="apple-touch-icon" sizes="72x72" href="http://placehold.it/72.png/000/fff">
    <!-- Standard iPhone Touch Icon-->
    <link rel="apple-touch-icon" sizes="57x57" href="http://placehold.it/57.png/000/fff">

    <!-- Styles -->
    <link href="assets/css/lib/weather-icons.css" rel="stylesheet" />
    <link href="assets/css/lib/owl.carousel.min.css" rel="stylesheet" />
    <link href="assets/css/lib/owl.theme.default.min.css" rel="stylesheet" />
    <link href="assets/css/lib/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/lib/themify-icons.css" rel="stylesheet">
    <link href="assets/css/lib/menubar/sidebar.css" rel="stylesheet">
    <link href="assets/css/lib/bootstrap.min.css" rel="stylesheet">

    <link href="assets/css/lib/helper.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">




</head>

<body>

    <!-- sidebar -->
    <div class="sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures">
        <div class="nano">
            <div class="nano-content">
                <div class="logo"><a href="index.php">
                        <!-- <img src="assets/images/logo.png" alt="" /> --><span>Pesona Wisata</span></a></div>
                <ul>
                    <!-- <li class="label">Main</li> -->
                    <!-- <span class="badge badge-primary">2</span>  biar ada angka -->
                    <li><a class="sidebar-sub-toggle"><i class="ti-home"></i> Wilayah <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                        <ul>
                            <li><a href="provinsi.php">Provinsi</a></li>
                            <li><a href="kabupaten.php">Kabupaten</a></li>
                            <li><a href="kecamatan.php">Kecamatan</a></li>
                        </ul>
                    </li>

                    <!-- <li class="label">Apps</li> -->
                    <li><a class="sidebar-sub-toggle"><i class="ti-bar-chart-alt"></i> Wisata <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                        <ul>
                            <li><a href="kategoriwisata.php">Kategori Wisata</a></li>
                            <li><a href="destinasiwisata.php">Destinasi Wisata</a></li>
                            <li><a href="fotowisata.php">Foto Wisata</a></li>
                        </ul>
                    </li>

                    <li><a href="hotel.php"><i class="ti-calendar"></i> Hotel </a></li>
                    <li><a href="restoran.php"><i class="ti-email"></i> Restoran</a></li>
                    <!-- <li><a href="app-profile.html"><i class="ti-user"></i> Berita</a></li> -->

                    <li><a class="sidebar-sub-toggle"><i class="ti-bar-chart-alt"></i> Berita <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                        <ul>
                            <li><a href="kategoriberita.php" class="">Kategori Berita</a></li>
                            <li><a href="isiberita.php" class="">Isi Berita</a></li>
                        </ul>


                    </li>

                    <li><a href="slider.php"><i class="ti-email"></i> Slider</a></li>


                </ul>
            </div>
        </div>
    </div>
    <!-- /# sidebar -->


    <!-- header atas -->
    <div class="header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="float-left">
                        <div class="hamburger sidebar-toggle">
                            <span class="line"></span>
                            <span class="line"></span>
                            <span class="line"></span>
                        </div>
                    </div>
                    <div class="float-right">
                        <ul>

                            <!-- <li class="header-icon dib"><i class="ti-bell"></i>
                                <div class="drop-down">
                                    <div class="dropdown-content-heading">
                                        <span class="text-left">Recent Notifications</span>
                                    </div>
                                    <div class="dropdown-content-body">
                                        <ul>
                                            <li>
                                                <a href="#">
                                                    <img class="pull-left m-r-10 avatar-img" src="assets/images/avatar/3.jpg" alt="" />
                                                    <div class="notification-content">
                                                        <small class="notification-timestamp pull-right">02:34 PM</small>
                                                        <div class="notification-heading">Mr. John</div>
                                                        <div class="notification-text">5 members joined today </div>
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <img class="pull-left m-r-10 avatar-img" src="assets/images/avatar/3.jpg" alt="" />
                                                    <div class="notification-content">
                                                        <small class="notification-timestamp pull-right">02:34 PM</small>
                                                        <div class="notification-heading">Mariam</div>
                                                        <div class="notification-text">likes a photo of you</div>
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <img class="pull-left m-r-10 avatar-img" src="assets/images/avatar/3.jpg" alt="" />
                                                    <div class="notification-content">
                                                        <small class="notification-timestamp pull-right">02:34 PM</small>
                                                        <div class="notification-heading">Tasnim</div>
                                                        <div class="notification-text">Hi Teddy, Just wanted to let you ...</div>
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <img class="pull-left m-r-10 avatar-img" src="assets/images/avatar/3.jpg" alt="" />
                                                    <div class="notification-content">
                                                        <small class="notification-timestamp pull-right">02:34 PM</small>
                                                        <div class="notification-heading">Mr. John</div>
                                                        <div class="notification-text">Hi Teddy, Just wanted to let you ...</div>
                                                    </div>
                                                </a>
                                            </li>
                                            <li class="text-center">
                                                <a href="#" class="more-link">See All</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </li> -->
                            <!-- <li class="header-icon dib"><i class="ti-email"></i>
                                <div class="drop-down">
                                    <div class="dropdown-content-heading">
                                        <span class="text-left">2 New Messages</span>
                                        <a href="email.html"><i class="ti-pencil-alt pull-right"></i></a>
                                    </div>
                                    <div class="dropdown-content-body">
                                        <ul>
                                            <li class="notification-unread">
                                                <a href="#">
                                                    <img class="pull-left m-r-10 avatar-img" src="assets/images/avatar/1.jpg" alt="" />
                                                    <div class="notification-content">
                                                        <small class="notification-timestamp pull-right">02:34 PM</small>
                                                        <div class="notification-heading">Michael Qin</div>
                                                        <div class="notification-text">Hi Teddy, Just wanted to let you ...</div>
                                                    </div>
                                                </a>
                                            </li>
                                            <li class="notification-unread">
                                                <a href="#">
                                                    <img class="pull-left m-r-10 avatar-img" src="assets/images/avatar/2.jpg" alt="" />
                                                    <div class="notification-content">
                                                        <small class="notification-timestamp pull-right">02:34 PM</small>
                                                        <div class="notification-heading">Mr. John</div>
                                                        <div class="notification-text">Hi Teddy, Just wanted to let you ...</div>
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <img class="pull-left m-r-10 avatar-img" src="assets/images/avatar/3.jpg" alt="" />
                                                    <div class="notification-content">
                                                        <small class="notification-timestamp pull-right">02:34 PM</small>
                                                        <div class="notification-heading">Michael Qin</div>
                                                        <div class="notification-text">Hi Teddy, Just wanted to let you ...</div>
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <img class="pull-left m-r-10 avatar-img" src="assets/images/avatar/2.jpg" alt="" />
                                                    <div class="notification-content">
                                                        <small class="notification-timestamp pull-right">02:34 PM</small>
                                                        <div class="notification-heading">Mr. John</div>
                                                        <div class="notification-text">Hi Teddy, Just wanted to let you ...</div>
                                                    </div>
                                                </a>
                                            </li>
                                            <li class="text-center">
                                                <a href="#" class="more-link">See All</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </li> -->


                            <li class="header-icon dib"><span class="user-avatar"><?= isset($_SESSION["username"]) ?  $_SESSION["username"] : "Admin"; ?> <i class="ti-angle-down f-s-10"></i></span>
                                <div class="drop-down dropdown-profile">
                                    <!-- <div class="dropdown-content-heading"> -->
                                    <!-- <span class="text-left">Upgrade Now</span>
                                        <p class="trial-day">30 Days Trail</p> -->
                                    <!-- <li><a href="#" class=""><i class="ti-user"><span>test</span></i></a></li> -->
                                    <!-- </div> -->
                                    <div class="dropdown-content-body">
                                        <ul>
                                            <!-- <li><a href="#"><i class="ti-user"></i> <span>Profile</span></a></li> -->

                                            <!-- <li><a href="#"><i class="ti-email"></i> <span>Inbox</span></a></li> -->
                                            <li><a href="myprofile.php"><i class="ti-settings"></i> <span>Profile</span></a></li>

                                            <!-- <li><a href="#"><i class="ti-lock"></i> <span>Lock Screen</span></a></li> -->
                                            <li><a href="logout.php"><i class="ti-power-off"></i> <span>Logout</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>