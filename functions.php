<?php

$connection = mysqli_connect('localhost', 'root', '', 'dbpesona');


function query($query)
{
    global $connection;

    $result = mysqli_query($connection, $query);

    $rows = [];
    while ($row = mysqli_fetch_assoc($result)) {
        $rows[] = $row;
    }

    return $rows;
}


function editkategoriwisata($data)
{
    global $connection;

    $kodekategoriwisata = htmlspecialchars($data["kategoriKODE"]);
    $namakategori = htmlspecialchars($data["kategoriNAMA"]);
    $kategoriketerangan = htmlspecialchars($data["kategoriKET"]);
    $referensikategori = htmlspecialchars($data["kategoriREFERENCE"]);

    $query = "UPDATE kategoriwisata SET
    kategoriKODE = '$kodekategoriwisata',
    kategoriNAMA = '$namakategori',
    kategoriKET = '$kategoriketerangan',
    kategoriREFERENCE = '$referensikategori'
    WHERE kategoriKODE = '$kodekategoriwisata'
    ";

    mysqli_query($connection, $query);
    return mysqli_affected_rows($connection);
}

function editkecamatan($data)
{
    global $connection;

    // ambil data
    $kodekecamatan = htmlspecialchars($data["KodeKecamatan"]);
    $namakecamatan = htmlspecialchars($data["NamaKecamatan"]);
    $alamatkecamatan = htmlspecialchars($data["AlamatKecamatan"]);
    $keterangankecamatan = htmlspecialchars($data["KeteranganKecamatan"]);
    $tanggalkecamatan = htmlspecialchars($data["TanggalKecamatan"]);
    $kabupatenkode = htmlspecialchars($data["kabupatenKODE"]);
    $gambarlama = htmlspecialchars($data["GambarLama"]);

    if ($_FILES['Gambar']['error'] === 4) {
        $gambar = $gambarlama;
    } else {
        $gambar = upload();
    }

    $query = "UPDATE kecamatan SET
    kecamatanKODE = '$kodekecamatan',
    kecamatanNAMA = '$namakecamatan',
    kecamatanALAMAT = '$keterangankecamatan',
    kecamatanTGL = '$tanggalkecamatan',
    kecamatanFOTO = '$gambar',
    kabupatenKODE = '$kabupatenkode'
    WHERE kecamatanKODE = '$kodekecamatan'
    ";

    mysqli_query($connection, $query);
    return mysqli_affected_rows($connection);
}

function editfotowisata($data)
{
    global $connection;

    $fotoobyekkode = htmlspecialchars($data["fotoobyekKODE"]);
    $fotoobyeknama = htmlspecialchars($data["fotoobyekNAMA"]);
    $obyekkode = htmlspecialchars($data["obyekKODE"]);
    $fotoobyekket = htmlspecialchars($data["fotoobyekKET"]);
    $fotoobyektglambil = htmlspecialchars($data["fotoobyekTGLAMBIL"]);
    $gambarlama = htmlspecialchars($data["GambarLama"]);

    if ($_FILES['Gambar']['error'] === 4) {
        $gambar = $gambarlama;
    } else {
        $gambar = upload();
    }

    $query = "UPDATE fotoobyekwisata SET
    fotoobyekKODE = '$fotoobyekkode',
    fotoobyekNAMA = '$fotoobyeknama',
    obyekKODE = '$obyekkode',
    fotoobyekKET = '$fotoobyekket',
    fotoobyekTGLAMBIL = '$fotoobyektglambil',
    fotoobyekGAMBAR = '$gambar'
    WHERE fotoobyekKODE = '$fotoobyekkode'
    ";

    mysqli_query($connection, $query);
    return mysqli_affected_rows($connection);
}

function editdestinasiwisata($data)
{
    global $connection;
    $obyekkode = htmlspecialchars($data["obyekKODE"]);
    $obyeknama = htmlspecialchars($data["obyekNAMA"]);
    $kecamatankode = htmlspecialchars($data["kecamatanKODE"]);
    $kategorikode = htmlspecialchars($data["kategoriKODE"]);
    $obyekalamat = htmlspecialchars($data["obyekALAMAT"]);
    $obyekderajat_s = htmlspecialchars($data["obyekDERAJAT_S"]);
    $obyekmenit_s = htmlspecialchars($data["obyekMENIT_S"]);
    $obyekdetik_s = htmlspecialchars($data["obyekDETIK_S"]);
    $obyeklatitude = htmlspecialchars($data["obyekLATITUDE"]);
    $obyekderajat_e = htmlspecialchars($data["obyekDERAJAT_E"]);
    $obyekmenit_e = htmlspecialchars($data["obyekMENIT_E"]);
    $obyekdetik_e = htmlspecialchars($data["obyekDETIK_E"]);
    $obyeklongitude = htmlspecialchars($data["obyekLONGITUDE"]);
    $obyekketinggian = htmlspecialchars($data["obyekKETINGGIAN"]);
    $obyekdefinisi = htmlspecialchars($data["obyekDEFINISI"]);
    $obyekketerangan = htmlspecialchars($data["obyekKETERANGAN"]);
    $gambarlama = htmlspecialchars($data["GambarLama"]);

    if ($_FILES['Gambar']['error'] === 4) {
        $gambar = $gambarlama;
    } else {
        $gambar = upload();
    }

















    $query = "UPDATE obyekwisata SET 
    obyekKODE = '$obyekkode',
    obyekNAMA ='$obyeknama',
    kecamatanKODE ='$kecamatankode',
    kategoriKODE ='$kategorikode',
    obyekALAMAT ='$obyekalamat',
    obyekDERAJAT_S ='$obyekderajat_s',
    obyekMENIT_S ='$obyekmenit_s',
    obyekDETIK_S ='$obyekdetik_s',
    obyekLATITUDE ='$obyeklatitude',
    obyekDERAJAT_E ='$obyekderajat_e',
    obyekMENIT_E ='$obyekmenit_e',
    obyekDETIK_E ='$obyekdetik_e',
    obyekLONGITUDE ='$obyeklongitude',
    obyekKETINGGIAN ='$obyekketinggian',
    obyekDEFINISI ='$obyekdefinisi',
    obyekKETERANGAN ='$obyekketerangan',
    obyekFOTO ='$gambar'
    WHERE obyekKODE = '$obyekkode'
    ";

    mysqli_query(
        $connection,
        $query
    );
    return mysqli_affected_rows($connection);
}

function edithotel($data)
{
    global $connection;

    $id = $_GET["nomor"];

    $kecamatankode = htmlspecialchars($data["kecamatanKODE"]);
    $namahotel = htmlspecialchars($data["namahotel"]);
    $bintanghotel = htmlspecialchars($data["bintanghotel"]);
    $keteranganhotel = htmlspecialchars($data["keteranganhotel"]);
    $gambarlama = htmlspecialchars($data["GambarLama"]);

    if ($_FILES['Gambar']['error'] === 4) {
        $gambar = $gambarlama;
    } else {
        $gambar = upload();
    }

    $query = "UPDATE hotel SET
    kecamatanKODE = '$kecamatankode',
    namahotel = '$namahotel',
    bintanghotel = '$bintanghotel',
    keteranganhotel = '$keteranganhotel',
    fotohotel = '$gambar'
    WHERE nomor = '$id'
    ";

    mysqli_query($connection, $query);
    return mysqli_affected_rows($connection);
}

function editrestoran($data)
{
    global $connection;

    $id = $_GET["nomor"];
    $kecamatankode = htmlspecialchars($data["kecamatanKODE"]);
    $namarestoran = htmlspecialchars($data["namarestoran"]);
    $keteranganrestoran = htmlspecialchars($data["keteranganrestoran"]);
    $gambarlama = htmlspecialchars($data["GambarLama"]);

    if ($_FILES['Gambar']['error'] === 4) {
        $gambar = $gambarlama;
    } else {
        $gambar = upload();
    }

    $query = "UPDATE restoran SET
    kecamatanKODE = '$kecamatankode',
    namarestoran = '$namarestoran',
    keteranganrestoran = '$keteranganrestoran',
    fotorestoran = '$gambar' WHERE nomor = '$id'
    ";


    mysqli_query($connection, $query);
    return mysqli_affected_rows($connection);
}

function editkabupaten($data)
{
    global $connection;

    // ambil data
    $kodekabupaten = htmlspecialchars($data["KodeKabupaten"]);
    $namakabupaten = htmlspecialchars($data["NamaKabupaten"]);
    $alamatkabupaten = htmlspecialchars($data["AlamatKabupaten"]);
    $keterangankabupaten = htmlspecialchars($data["KeteranganKabupaten"]);
    $gambarlama = htmlspecialchars($data["GambarLama"]);
    $keteranganfoto = htmlspecialchars($data["kabupatenFOTOICONKET"]);


    if ($_FILES['Gambar']['error'] === 4) {
        $gambar = $gambarlama;
    } else {
        $gambar = upload();
    }

    $query = "UPDATE kabupaten SET
    kabupatenKODE = '$kodekabupaten',
    kabupatenNAMA = '$namakabupaten',
    kabupatenALAMAT = '$alamatkabupaten',
    kabupatenKET = '$keterangankabupaten',
    kabupatenFOTOICON = '$gambar',
    kabupatenFOTOICONKET = '$keteranganfoto'
    WHERE kabupatenKODE = '$kodekabupaten'
    ";


    mysqli_query($connection, $query);
    return mysqli_affected_rows($connection);
}

// function editkabupaten($data)
// {
//     global $connection;

//     // ambil data
//     $kodekabupaten = htmlspecialchars($data["KodeKabupaten"]);
//     $namakabupaten = htmlspecialchars($data["NamaKabupaten"]);
//     $alamatkabupaten = htmlspecialchars($data["AlamatKabupaten"]);
//     $keterangankabupaten = htmlspecialchars($data["KeteranganKabupaten"]);

//     $query = "UPDATE kabupaten SET
//     kabupatenKODE = '$kodekabupaten',
//     kabupatenNAMA = '$namakabupaten',
//     kabupatenALAMAT = '$alamatkabupaten',
//     kabupatenKET = '$keterangankabupaten',

//     "
// }

function editkategoriberita($data)
{
    global $connection;

    $kategoriberitakode = htmlspecialchars($data["kategoriberitaKODE"]);
    $kategoriberitanama = htmlspecialchars($data["kategoriberitaNAMA"]);
    $kategoriberitaket = htmlspecialchars($data["kategoriberitaKET"]);

    $query = ("UPDATE kategoriberita SET
    kategoriberitaKODE = '$kategoriberitakode',
    kategoriberitaNAMA = '$kategoriberitanama',
    kategoriberitaKET = '$kategoriberitaket' where kategoriberitaKODE = '$kategoriberitakode'
    ");

    mysqli_query($connection, $query);
    return mysqli_affected_rows($connection);
}

function editisiberita($data)
{
    global $connection;
    $beritakode = htmlspecialchars($data["beritaKODE"]);
    $beritajudul = htmlspecialchars($data["beritaJUDUL"]);
    $kategoriberitakode = htmlspecialchars($data["kategoriberitaKODE"]);
    $eventkode = htmlspecialchars($data["eventKODE"]);
    $kabupatenkode = htmlspecialchars($data["kabupatenKODE"]);
    $beritaisi = htmlspecialchars($data["beritaISI"]);
    $beritaisi2 = htmlspecialchars($data["beritaISI2"]);
    $beritasumber = htmlspecialchars($data["beritaSUMBER"]);
    $beritapenulis = htmlspecialchars($data["beritaPENULIS"]);
    $beritatgl = htmlspecialchars($data["tanggalberita"]);
    $gambarlama = htmlspecialchars($data["GambarLama"]);

    if ($_FILES['Gambar']['error'] === 4) {
        $gambar = $gambarlama;
    } else {
        $gambar = upload();
    }

    $query = "UPDATE berita SET
    beritaKODE = '$beritakode',
    beritaJUDUL = '$beritajudul',
    kategoriberitaKODE = '$kategoriberitakode',
    eventKODE = '$eventkode',
    kabupatenKODE = '$kabupatenkode',
    beritaISI = '$beritaisi',
    beritaISI2 = '$beritaisi2',
    beritaSUMBER = '$beritasumber',
    beritaPENULIS = '$beritapenulis',
    beritaTGL = '$beritatgl',
    beritaICONFOTO = '$gambar' WHERE beritaKODE = '$beritakode'
    ";



    mysqli_query($connection, $query);
    return mysqli_affected_rows($connection);
}

function ubahkecamatan($data)
{
    global $connection;

    $kecamatankode = htmlspecialchars($data["KodeKecamatan"]);
    $kecamatannama = htmlspecialchars($data["NamaKecamatan"]);
    $kecamatanalamat = htmlspecialchars($data["AlamatKecamatan"]);
    $kecamatanket = htmlspecialchars($data["KeteranganKecamatan"]);
    $kecamatantgl = htmlspecialchars($data["TanggalKecamatan"]);
    // $kecamatanfoto = htmlspecialchars($data[""]);
    $kabupatenkode = htmlspecialchars($data["NamaKabupaten"]);
    $gambarlama = htmlspecialchars($data["gambarlama"]);

    if ($_FILES['GambarKecamatan']['error'] === 4) {
        $gambar = $gambarlama;
    } else {
        $gambar = upload();
    }

    // $gambar = htmlspecialchars($data["GambarKecamatan"]);


    $query = "UPDATE kecamatan SET
    kecamatanKODE = '$kecamatankode',
    kecamatanNAMA = '$kecamatannama',
    kecamatanALAMAT = '$kecamatanalamat',
    kecamatanKET = '$kecamatanket',
    kecamatanTGL = '$kecamatantgl',
    kecamatanFOTO = '$gambar',
    kabupatenKODE = '$kabupatenkode'
    WHERE kecamatanKODE = '$kecamatankode'
    ";



    return query($query);
}

function deletedata($getdata, $fromtable, $wheredata)
{

    global $connection;
    $delete = $getdata;

    $query = "DELETE FROM $fromtable WHERE $wheredata = '$delete'";
    mysqli_query($connection, $query);
    return mysqli_affected_rows($connection);
}

function insertslider($data)
{
    global $connection;

    $kodeslider = htmlspecialchars($data["sliderKODE"]);
    $judulslider = htmlspecialchars($data["sliderJUDUL"]);
    $sliderket = htmlspecialchars($data["sliderKET"]);

    $gambar = upload();
    if ($gambar === false) {
        return false;
    }
    $query = "INSERT INTO slider
    VALUES('$kodeslider','$gambar','$judulslider','$sliderket')
    ";

    mysqli_query($connection, $query);
    return mysqli_affected_rows($connection);
}

function insertrestoran($data)
{

    global $connection;


    // ambildata
    $kecamatankode = htmlspecialchars($data["kecamatanKODE"]);
    $namarestoran  = htmlspecialchars($data["namarestoran"]);
    $keteranganrestoran = htmlspecialchars($data["keteranganrestoran"]);


    $gambar = upload();
    if ($gambar === false) {
        return false;
    }

    $query = "INSERT INTO restoran
                VALUES('','$kecamatankode','$namarestoran','$keteranganrestoran','$gambar')";




    mysqli_query($connection, $query);
    return mysqli_affected_rows($connection);
}


function insertisiberita($data)
{
    global $connection;

    // ambildata
    $kodeberita = htmlspecialchars($data["beritaKODE"]);
    $beritajudul = htmlspecialchars($data["beritaJUDUL"]);
    $kategoriberitakode = htmlspecialchars($data["kategoriberitaKODE"]);
    $eventkode = htmlspecialchars($data["eventKODE"]);
    $kabupatenKODE = htmlspecialchars($data["kabupatenKODE"]);
    $isiberita = htmlspecialchars($data["beritaISI"]);
    $isiberita2 = htmlspecialchars($data["beritaISI2"]);
    $sumberberita = htmlspecialchars($data["beritaSUMBER"]);
    $penulisberita = htmlspecialchars($data["beritaPENULIS"]);
    $tanggalberita  = htmlspecialchars($data["tanggalberita"]);








    // uploadgambar
    $gambar =  upload();
    if ($gambar === false) {
        return false;
    }


    $query = "INSERT INTO berita
    VALUES('$kodeberita','$beritajudul','$kategoriberitakode','$eventkode','$kabupatenKODE','$isiberita','$isiberita2','$sumberberita','$penulisberita','$tanggalberita','$gambar')";

    // $query = "INSERT INTO berita
    // VALUES('$kodeberita','$beritajudul','$kategoriberitakode','$eventkode','$kabupatenKODE','$isiberita','$isiberita2','$sumberberita','$penulisberita','$tanggalberita,'$gambar')";

    mysqli_query($connection, $query);
    return (mysqli_affected_rows($connection));
}


function insertkecamatan($data)
{
    global $connection;

    // ambil data
    $kodekecamatan = htmlspecialchars($data["KodeKecamatan"]);
    $namakecamatan = htmlspecialchars($data["NamaKecamatan"]);
    $alamatkecamatan = htmlspecialchars($data["AlamatKecamatan"]);
    $keterangankecamatan = htmlspecialchars($data["KeteranganKecamatan"]);
    $tanggalkecamatan = htmlspecialchars($data["TanggalKecamatan"]);
    $namakabupaten = htmlspecialchars($data["NamaKabupaten"]);



    // $gambarkecamatan = htmlspecialchars($data["UploadGambar"]);


    // upload gambar
    $gambar =  upload();
    if ($gambar === false) {
        return false;
    }




    $query = "INSERT INTO kecamatan
    VALUES('$kodekecamatan','$namakecamatan','$alamatkecamatan','$keterangankecamatan','$tanggalkecamatan','$gambar','$namakabupaten')";

    mysqli_query($connection, $query);
    return mysqli_affected_rows($connection);
}

function insertprovinsi($data)
{
    global $connection;

    // ambil data
    $provinsikode = htmlspecialchars($data["provinsiKODE"]);
    $provinsinama = htmlspecialchars($data["provinsiNAMA"]);
    $kabupatenkode = htmlspecialchars($data["kabupatenKODE"]);
    $kecamatankode = htmlspecialchars($data["kecamatanKODE"]);
    $ibukota = htmlspecialchars($data["ibukota"]);
    $luaswilayah = htmlspecialchars($data["luaswilayah"]);
    $gambar = upload();
    if (!$gambar) {
        return false;
    }


    $query = "INSERT INTO provinsi VALUES('$provinsikode','$provinsinama','$kabupatenkode','$kecamatankode','$ibukota','$luaswilayah','$gambar')";


    mysqli_query($connection, $query);
    return mysqli_affected_rows($connection);
}

function upload()
{

    $namafile = $_FILES['Gambar']['name'];
    $ukuranfile = $_FILES['Gambar']['size'];
    $error = $_FILES['Gambar']['error'];
    $tmpname = $_FILES['Gambar']['tmp_name'];


    if ($error === 4) {
        echo "
        <script>
            alert('pilih gambar dulu');
        </script>
        
        ";
        return false;
    }



    // imageonly checker
    $imageextension = ['jpg', 'jpeg', 'png'];
    // delimiter
    $ekstensigambar = explode('.', $namafile);
    // force small character dan ambil ekstensi aja
    $ekstensigambar = strtolower(end($ekstensigambar));




    if (!in_array($ekstensigambar, $imageextension)) {
        echo "<script>
        alert('yang di upload bukan gambar');
        </script>";
        return false;
    }


    // still not working
    if ($ukuranfile > 3000000) {

        echo "<script>
        alert('cek ukuran file');
        </script>";
        return false;
    }

    $namafilebaru = uniqid();
    $namafilebaru .= '.';
    $namafilebaru .= $ekstensigambar;
    // var_dump($namafilebaru);

    // pindah gambar 
    move_uploaded_file($tmpname, 'img/' . $namafilebaru);
    return $namafilebaru;
}

// function ubahkategoriwisata($data)
// {
//     global $connection;
//     $kodekategoriwisata = htmlspecialchars($data["KategoriKodeWisata"]);
//     $namakategori = htmlspecialchars($data["NamaKategori"]);
//     $kategoriketerangan = htmlspecialchars($data["KategoriKeterangan"]);
//     $referensikategori = htmlspecialchars($data["ReferensiKategori"]);


//     $query = "UPDATE kategoriwisata SET
//     kategoriKODE = '$kodekategoriwisata',
//     kategoriNAMA = '$namakategori',
//     kategoriKET = '$kategoriketerangan',
//     kategoriREFERENCE = '$referensikategori'
//     WHERE kategoriKODE = '$kodekategoriwisata'
//     ";

//     return query($query);
// }


function editprovinsi($data)
{
    global $connection;
    $kodeprovinsi = htmlspecialchars($data["provinsiKODE"]);
    $namaprovinsi = htmlspecialchars($data["provinsiNAMA"]);
    $kodekabupaten = htmlspecialchars($data["kabupatenKODE"]);
    $kodekecamatan = htmlspecialchars($data["kecamatanKODE"]);
    $ibukota = htmlspecialchars($data["ibukota"]);
    $luaswilayah = htmlspecialchars($data["luaswilayah"]);
    $gambarlama = htmlspecialchars($data["GambarLama"]);

    if ($_FILES['Gambar']['error'] === 4) {
        $gambar = $gambarlama;
    } else {
        $gambar = upload();
    }


    $query = "UPDATE provinsi SET
    provinsiKODE = '$kodeprovinsi',
    provinsiNAMA = '$namaprovinsi',
    kabupatenKODE = '$kodekabupaten',
    kecamatanKODE = '$kodekecamatan',
    ibukota = '$ibukota',
    luaswilayah = '$luaswilayah',
    foto = '$gambar'
    WHERE provinsiKODE = '$kodeprovinsi'
    ";

    // var_dump($query);
    // die;


    mysqli_query($connection, $query);
    return mysqli_affected_rows($connection);
}

function inserthotel($data)
{
    global $connection;

    // ambil data
    $kecamatankode = htmlspecialchars($data["kecamatanKODE"]);
    $namahotel = htmlspecialchars($data["namahotel"]);
    $bintanghotel = htmlspecialchars($data["bintanghotel"]);
    $keteranganhotel = htmlspecialchars($data["keteranganhotel"]);

    $gambar = upload();

    if (!$gambar) {
        return false;
    }

    $query = "INSERT INTO hotel VALUES('','$kecamatankode','$namahotel','$bintanghotel','$keteranganhotel','$gambar')";

    mysqli_query($connection, $query);

    return (mysqli_affected_rows($connection));
}

function insertkabupaten($data)
{

    global $connection;

    // ambil data
    $kodekabupaten = htmlspecialchars($data["KodeKabupaten"]);
    $namakabupaten = htmlspecialchars($data["NamaKabupaten"]);
    $alamatkabupaten = htmlspecialchars($data["AlamatKabupaten"]);
    $keterangankabupaten = htmlspecialchars($data["KeteranganKabupaten"]);
    $keteranganfoto = htmlspecialchars($data["kabupatenFOTOICONKET"]);


    $gambar = upload();

    if (!$gambar) {
        return false;
    }


    $query = "INSERT INTO kabupaten
    VALUES('$kodekabupaten','$namakabupaten','$alamatkabupaten','$keterangankabupaten','$gambar','$keteranganfoto')";

    mysqli_query($connection, $query);
    return mysqli_affected_rows($connection);
}

function insertdesdinasiwisata($data)
{
    global $connection;


    // ambil data
    $obyekkode = htmlspecialchars($data["obyekKODE"]);
    $obyeknama = htmlspecialchars($data["obyekNAMA"]);
    $kecamatankode = htmlspecialchars($data["kecamatanKODE"]);
    $kategorikode = htmlspecialchars($data["kategoriKODE"]);
    $obyekalamat = htmlspecialchars($data["obyekALAMAT"]);
    $obyekderajat_s = htmlspecialchars($data["obyekDERAJAT_S"]);
    $obyekmenit_s = htmlspecialchars($data["obyekMENIT_S"]);
    $obyekdetik_s = htmlspecialchars($data["obyekDETIK_S"]);
    $obyeklatitude = htmlspecialchars($data["obyekLATITUDE"]);
    $obyekderajat_e = htmlspecialchars($data["obyekDERAJAT_E"]);
    $obyekmenit_e = htmlspecialchars($data["obyekMENIT_E"]);
    $obyekdetik_e = htmlspecialchars($data["obyekDETIK_E"]);
    $obyeklongitude = htmlspecialchars($data["obyekLONGITUDE"]);
    $obyekketinggian = htmlspecialchars($data["obyekKETINGGIAN"]);
    $obyekdefinisi = htmlspecialchars($data["obyekDEFINISI"]);
    $obyekketerangan = htmlspecialchars($data["obyekKETERANGAN"]);

    $gambar = upload();

    if (!$gambar) {
        return false;
    }

    // $query = "INSERT INTO obyekwisata VALUES('$obyekkode','$obyeknama','$kecamatankode','$kategorikode','$obyekalamat','$obyekderajat_s','$obyekmenit_s','$obyekdetik_s','$obyeklatitude','$obyekderajat_e','$obyekmenit_e','$obyekdetik_e','$obyeklongitude','$obyekketinggian','$obyekdefinisi','$obyekketerangan','')";
    $query = "INSERT INTO obyekwisata VALUES('$obyekkode','$obyeknama','$kecamatankode','$kategorikode','$obyekalamat','$obyekderajat_s','$obyekmenit_s','$obyekdetik_s','$obyeklatitude','$obyekderajat_e','$obyekmenit_e','$obyekdetik_e','$obyeklongitude','$obyekketinggian','$obyekdefinisi','$obyekketerangan','$gambar')";

    mysqli_query($connection, $query);
    return mysqli_affected_rows($connection);
}

function insertfotowisata($data)
{


    global $connection;

    $fotoobyekkode = htmlspecialchars($data["fotoobyekKODE"]);
    $fotoobyeknama = htmlspecialchars($data["fotoobyekNAMA"]);
    $obyekkode = htmlspecialchars($data["obyekKODE"]);
    $fotoobyekket = htmlspecialchars($data["fotoobyekKET"]);
    $fotoobyektglambil = htmlspecialchars($data["fotoobyekTGLAMBIL"]);



    $gambar = upload();
    if (!$gambar) {
        return false;
    }


    $query =  "INSERT INTO fotoobyekwisata VALUES('$fotoobyekkode','$fotoobyeknama','$obyekkode','$fotoobyekket','$fotoobyektglambil','$gambar')";

    mysqli_query($connection, $query);
    return mysqli_affected_rows($connection);
}


function insertkategoriberita($data)
{
    global $connection;


    // ambil data
    $kodekategori = htmlspecialchars($data["kategoriberitaKODE"]);
    $namakategori = htmlspecialchars($data["kategoriberitaNAMA"]);
    $keterangankategori = htmlspecialchars($data["kategoriberitaKET"]);

    $query = "INSERT INTO kategoriberita
    VALUES('$kodekategori','$namakategori','$keterangankategori')";

    mysqli_query($connection, $query);
    return mysqli_affected_rows($connection);
    // return (query($query));
}



function insertkategoriwisata($data)
{

    global $connection;

    $kodekategoriwisata = htmlspecialchars($data["kategoriKODE"]);
    $namakategori = htmlspecialchars($data["kategoriNAMA"]);
    $kategoriketerangan = htmlspecialchars($data["kategoriKET"]);
    $referensikategori = htmlspecialchars($data["kategoriREFERENCE"]);

    $query  = "INSERT INTO kategoriwisata VALUES('$kodekategoriwisata','$namakategori','$kategoriketerangan','$referensikategori')";


    mysqli_query($connection, $query);
    return mysqli_affected_rows($connection);
}


function ubahkategoriwisata($data)
{
    global $connection;
    $kodekategoriwisata = htmlspecialchars($data["KategoriKodeWisata"]);
    $namakategori = htmlspecialchars($data["NamaKategori"]);
    $kategoriketerangan = htmlspecialchars($data["KategoriKeterangan"]);
    $referensikategori = htmlspecialchars($data["ReferensiKategori"]);


    $query = "UPDATE kategoriwisata SET
    kategoriKODE = '$kodekategoriwisata',
    kategoriNAMA = '$namakategori',
    kategoriKET = '$kategoriketerangan',
    kategoriREFERENCE = '$referensikategori'
    WHERE kategoriKODE = '$kodekategoriwisata'
    ";

    return query($query);
}




function hapuskategoriwisata($id)
{


    global $connection;

    mysqli_query($connection, "DELETE FROM kategoriwisata WHERE kategoriKODE = '$id'");
    return mysqli_affected_rows($connection);
}


function generate_string($input, $strength = 16)
{
    $input_length = strlen($input);
    $random_string = '';
    for ($i = 0; $i < $strength; $i++) {
        $random_character = $input[mt_rand(0, $input_length - 1)];
        $random_string .= $random_character;
    }

    return $random_string;
}

function registrasi($data)
{

    global $connection;

    $username = strtolower(stripslashes($data["username"]));
    $password = mysqli_real_escape_string($connection, $data["password"]);
    $password2 = mysqli_real_escape_string($connection, $data["password2"]);


    // cek username udah ada atau belom
    $result = mysqli_query($connection, "SELECT NAMAadmin FROM admin2 WHERE NAMAadmin = '$username'");
    if (mysqli_fetch_assoc($result)) {
        echo "
    <script>
    alert('username exists')
    </script>
    ";
        return false;
    }

    // cek konfirmasi password
    if ($password != $password2) {
        echo "
        <script>
        alert('password berbeda');
        </script>
        ";
    } else {
        echo mysqli_error($connection);
    }


    // primary key utk tabel
    $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

    $randomid = generate_string($permitted_chars, 4);

    // enkripsi password

    $password = password_hash($password, PASSWORD_DEFAULT);

    // tambah user ke database
    mysqli_query($connection, "INSERT INTO admin2 VALUES(
        '$randomid','$username','$password'
    )");

    return mysqli_affected_rows($connection);
}
