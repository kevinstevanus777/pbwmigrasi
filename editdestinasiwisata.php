<?php
include "include/_header.php";

$selectkecamatan = query("SELECT * FROM kecamatan");

$selectkategoriwisata = query("SELECT * FROM kategoriwisata");
if (isset($_POST["submit"])) {



    if (editdestinasiwisata($_POST) > 0) {
        echo "<script>
    alert('data berhasil ditambahkan!');
    document.location.href = 'destinasiwisata.php'
</script>";
    } else {
        echo "<script>
    alert('data tidak berhasil ditambahkan!');
    document.location.href = 'destinasiwisata.php'
</script>";
    }
}


$id = $_GET["obyekKODE"];

$selectdestinasiwisata = query("SELECT * FROM obyekwisata WHERE obyekKODE = '$id'")[0];



?>



<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">

            <!-- /# row -->
            <section id="main-content">




                <div class="row">
                    <div class="col-sm">
                        <div class="card">
                            <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">

                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-6">

                                            <div class='form-group'>
                                                <label for='obyekKODE'>Kode Obyek</label>
                                                <input type='text' class='form-control' id='obyekKODE' name='obyekKODE' placeholder='Kode Obyek' value="<?= $selectdestinasiwisata["obyekKODE"]; ?>">
                                            </div>

                                            <div class='form-group'>
                                                <label for='obyekNAMA'>Nama Obyek</label>
                                                <input type='text' class='form-control' id='obyekNAMA' name='obyekNAMA' placeholder='Nama Obyek' value="<?= $selectdestinasiwisata["obyekNAMA"]; ?>">
                                            </div>






                                            <div class='form-group'>
                                                <label for='kecamatanKODE'>Nama Kecamatan</label>
                                                <select name='kecamatanKODE' id='kecamatanKODE' class='form-control'>
                                                    <?php foreach ($selectkecamatan as $kecamatan) : ?>
                                                        <option value='<?php echo $kecamatan['kecamatanKODE']; ?>' <?php if ($kecamatan['kecamatanKODE'] === $selectdestinasiwisata['kecamatanKODE']) {
                                                                                                                            echo 'selected';
                                                                                                                        } ?>>
                                                            <?php echo $kecamatan['kecamatanNAMA']; ?>
                                                        </option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>




                                            <div class='form-group'>
                                                <label for='kategoriKODE'>Nama Kategori Wisata</label>
                                                <select name='kategoriKODE' id='kategoriKODE' class='form-control'>
                                                    <?php foreach ($selectkategoriwisata as $kategori) : ?>
                                                        <option value='<?php echo $kategori['kategoriKODE']; ?>' <?php if ($kategori['kategoriKODE'] === $selectdestinasiwisata['kategoriKODE']) {
                                                                                                                            echo 'selected';
                                                                                                                        } ?>>
                                                            <?php echo $kategori['kategoriNAMA']; ?>
                                                        </option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>


                                            <div class='form-group'>
                                                <label for='obyekALAMAT'>Alamat Obyek</label>
                                                <input type='text' class='form-control' id='obyekALAMAT' name='obyekALAMAT' placeholder='Alamat Obyek' value="<?= $selectdestinasiwisata['obyekALAMAT']; ?>">
                                            </div>


                                            <div class='form-group'>
                                                <label for='obyekDERAJAT_S'>Obyek Derajat S</label>
                                                <input type='text' class='form-control' id='obyekDERAJAT_S' name='obyekDERAJAT_S' placeholder='Obyek Derajat S' value="<?= $selectdestinasiwisata['obyekDERAJAT_S']; ?>">
                                            </div>


                                            <div class='form-group'>
                                                <label for='obyekMENIT_S'>Obyek Menit S</label>
                                                <input type='text' class='form-control' id='obyekMENIT_S' name='obyekMENIT_S' placeholder='Obyek Menit S' value="<?= $selectdestinasiwisata['obyekMENIT_S']; ?>">
                                            </div>

                                            <div class='form-group'>
                                                <label for='obyekDETIK_S'>Obyek Detik S</label>
                                                <input type='text' class='form-control' id='obyekDETIK_S' name='obyekDETIK_S' placeholder='Obyek Detik S' value="<?= $selectdestinasiwisata['obyekDETIK_S']; ?>">
                                            </div>




                                        </div>



                                        <div class="col-md-6">
                                            <div class='form-group'>
                                                <label for='obyekLATITUDE'>Obyek Latitude</label>
                                                <input type='text' class='form-control' id='obyekLATITUDE' name='obyekLATITUDE' placeholder='Obyek Latitude' value="<?= $selectdestinasiwisata["obyekLATITUDE"]; ?>">
                                            </div>
                                            <div class='form-group'>
                                                <label for='obyekDERAJAT_E'>Obyek Derajat E</label>
                                                <input type='text' class='form-control' id='obyekDERAJAT_E' name='obyekDERAJAT_E' placeholder='Obyek Derajat E' value="<?= $selectdestinasiwisata["obyekDERAJAT_E"]; ?>">
                                            </div>
                                            <div class='form-group'>
                                                <label for='obyekMENIT_E'>Obyek Menit E</label>
                                                <input type='text' class='form-control' id='obyekMENIT_E' name='obyekMENIT_E' placeholder='Obyek Menit E' value="<?= $selectdestinasiwisata["obyekMENIT_E"]; ?>">
                                            </div>

                                            <div class='form-group'>
                                                <label for='obyekDETIK_E'>Obyek Detik E</label>
                                                <input type='text' class='form-control' id='obyekDETIK_E' name='obyekDETIK_E' placeholder='Obyek Detik E' value="<?= $selectdestinasiwisata["obyekDETIK_E"]; ?>">
                                            </div>

                                            <div class='form-group'>
                                                <label for='obyekLONGITUDE'>Obyek Longitude</label>
                                                <input type='text' class='form-control' id='obyekLONGITUDE' name='obyekLONGITUDE' placeholder='Obyek Longitude' value="<?= $selectdestinasiwisata["obyekLONGITUDE"]; ?>">
                                            </div>

                                            <div class='form-group'>
                                                <label for='obyekKETINGGIAN'>Obyek Ketinggian</label>
                                                <input type='text' class='form-control' id='obyekKETINGGIAN' name='obyekKETINGGIAN' placeholder='Obyek Ketinggian' value="<?= $selectdestinasiwisata["obyekKETINGGIAN"]; ?>">
                                            </div>


                                            <div class='form-group'>
                                                <label for='obyekDEFINISI'>Obyek Definisi</label>
                                                <input type='text' class='form-control' id='obyekDEFINISI' name='obyekDEFINISI' placeholder='Obyek Definisi' value="<?= $selectdestinasiwisata["obyekDEFINISI"]; ?>">
                                            </div>

                                            <div class='form-group'>
                                                <label for='obyekKETERANGAN'>Keterangan</label>
                                                <input type='text' class='form-control' id='obyekKETERANGAN' name='obyekKETERANGAN' placeholder='Keterangan' value="<?= $selectdestinasiwisata["obyekKETERANGAN"]; ?>">
                                            </div>


                                            <div class='form-group'>
                                                <label for='Gambar'>Upload Gambar</label>
                                                <input type='file' id='Gambar' name='Gambar'>
                                            </div>
                                            <input type="hidden" name="GambarLama" value="<?= $selectdestinasiwisata["obyekFOTO"]; ?>">

                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                                </div>
                            </form>


                        </div>
                    </div>
                </div>





            </section>

        </div>
    </div>
</div>




<?php include "include/_footer.php"; ?>