<?php
include "include/_header.php";

// pagination


// $selectkecamatan = query("SELECT * FROM kecamatan");
$selectkabupaten = query("SELECT * FROM kabupaten");

if (isset($_POST["submit"])) {



    if (editkecamatan($_POST) > 0) {
        echo "<script>
    alert('data berhasil ditambahkan!');
    document.location.href = ''
</script>";
    } else {
        echo "<script>
    alert('data tidak berhasil ditambahkan!');
    document.location.href = ''
</script>";
    }
}


$id = $_GET["kecamatanKODE"];
$selectkecamatan = query("SELECT * FROM kecamatan where kecamatanKODE = '$id'")[0];


?>

<!-- content -->
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">


            <!-- /# row -->
            <section id="main-content">




                <div class="row">
                    <div class="col-sm">
                        <div class="card">
                            <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">


                                <div class='form-group'>
                                    <label for='KodeKecamatan'>KodeKecamatan</label>
                                    <input type='text' class='form-control' id='KodeKecamatan' name='KodeKecamatan' placeholder='KodeKecamatan' value="<?= $selectkecamatan["kecamatanKODE"]; ?>">
                                </div>

                                <div class='form-group'>
                                    <label for='NamaKecamatan'>NamaKecamatan</label>
                                    <input type='text' class='form-control' id='NamaKecamatan' name='NamaKecamatan' placeholder='NamaKecamatan' value="<?= $selectkecamatan["kecamatanNAMA"]; ?>">
                                </div>

                                <div class='form-group'>
                                    <label for='AlamatKecamatan'>AlamatKecamatan</label>
                                    <input type='text' class='form-control' id='AlamatKecamatan' name='AlamatKecamatan' placeholder='AlamatKecamatan' value="<?= $selectkecamatan["kecamatanALAMAT"]; ?>">
                                </div>


                                <div class="form-group">
                                    <label for="KeteranganKecamatan">Keterangan Kecamatan</label>
                                    <textarea class="form-control" id="KeteranganKecamatan" name="KeteranganKecamatan" placeholder="Keterangan" rows="6"><?= $selectkecamatan["kecamatanKET"]; ?></textarea>
                                </div>

                                <div class='form-group'>
                                    <label for='datepicker'>TanggalKecamatan</label>

                                    <input type='text' class='form-control' id='datepicker' name='TanggalKecamatan' placeholder='TanggalKecamatan' value="<?= $selectkecamatan["kecamatanTGL"]; ?>">
                                    <script></script>

                                </div>



                                <!-- <div class='form-group'>
                                    <label for='NamaKabupaten'>NamaKabupaten</label>
                                    <select name="NamaKabupaten" id="NamaKabupaten" class="form-control">

                                        <?php foreach ($selectkabupaten as $kabupaten) : ?>

                                            <option value="<?= $kabupaten["kabupatenKODE"]; ?>">
                                                <?= $kabupaten["kabupatenNAMA"]; ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div> -->

                                <div class='form-group'>
                                    <label for='kabupatenKODE'>Nama Kabupaten</label>
                                    <select name='kabupatenKODE' id='kabupatenKODE' class='form-control'>
                                        <?php foreach ($selectkabupaten as $kabupaten) : ?>
                                            <option value='<?php echo $kabupaten['kabupatenKODE']; ?>' <?php if ($kabupaten['kabupatenKODE'] === $selectkecamatan['kabupatenKODE']) {
                                                                                                                echo 'selected';
                                                                                                            } ?>>
                                                <?php echo $kabupaten['kabupatenNAMA']; ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>



                                <div class='form-group'>
                                    <label for='Gambar'>Upload Gambar</label>
                                    <input type='file' id='Gambar' name='Gambar'>
                                </div>
                                <input type="hidden" name="GambarLama" value="<?= $selectkecamatan["kecamatanFOTO"]; ?>">


                                <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                            </form>

                        </div>
                    </div>
                </div>


            </section>
        </div>
    </div>
</div>

<?php include "include/_footer.php"; ?>