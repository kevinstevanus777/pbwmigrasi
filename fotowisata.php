<?php
include "include/_header.php";


if (isset($_POST["submit"])) {
    if (insertfotowisata($_POST) > 0) {
        echo "<script>
        alert('data berhasil ditambahkan!');
        document.location.href= ''
        </script>";
    } else {
        echo "<script>
        alert('data berhasil ditambahkan!');
        document.location.href= ''
        </script>";
    }
}



if (isset($_GET["fotoobyekKODE"])) {


    if (deletedata($_GET["fotoobyekKODE"], "fotoobyekWISATA", "fotoobyekKODE") > 0) {
        echo "<script>
    alert('data berhasil didelete!');
    document.location.href = 'fotowisata.php'
</script>";
    } else {
        echo "<script>
        alert('data tidak berhasil didelete!');
        document.location.href = 'fotowisata.php'
    </script>";
    }
}

$selectkabupaten =  query("SELECT * FROM kabupaten");
$selectwisata = query("SELECT * FROM obyekwisata");
$dataPerPagination = 5;
$jumlahTotalIndex = count(query("SELECT * FROM fotoobyekwisata"));
$jumlahHalaman = ceil($jumlahTotalIndex / $dataPerPagination);
$activePage = (isset($_GET["page"])) ? $_GET["page"] : 1;
$awalData = ($dataPerPagination * $activePage) - $dataPerPagination;
$selectfoto = query("SELECT * FROM fotoobyekwisata LIMIT $awalData , $dataPerPagination");


?>



<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">

            <!-- /# row -->
            <section id="main-content">


                <div class="row">
                    <div class="col-md-auto">
                        <div class="card">
                            <div class="card-body">
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                                    Insert Foto Wisata
                                </button>


                                <!-- Modal -->
                                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Input Kabupaten</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">



                                                    <div class='form-group'>
                                                        <label for='fotoobyekKODE'>Kode Foto</label>
                                                        <input type='text' class='form-control' id='fotoobyekKODE' name='fotoobyekKODE' placeholder='Kode Foto'>
                                                    </div>

                                                    <div class='form-group'>
                                                        <label for='fotoobyekNAMA'>Nama Foto</label>
                                                        <input type='text' class='form-control' id='fotoobyekNAMA' name='fotoobyekNAMA' placeholder='Nama Foto'>
                                                    </div>



                                                    <div class='form-group'>
                                                        <label for='obyekKODE'>Kode Obyek</label>
                                                        <select name="obyekKODE" id="obyekKODE" class="form-control">

                                                            <?php foreach ($selectwisata as $wisata) : ?>
                                                                <option value="<?= $wisata["obyekKODE"]; ?>">
                                                                    <?= $wisata["obyekNAMA"]; ?>
                                                                </option>
                                                            <?php endforeach; ?>

                                                        </select>
                                                    </div>

                                                    <div class='form-group'>
                                                        <label for='kabupatenKODE'>Nama Kabupaten</label>
                                                        <select name="kabupatenKODE" id="kabupatenKODE" class="form-control">

                                                            <?php foreach ($selectkabupaten as $kabupaten) : ?>

                                                                <option value="<?= $kabupaten["kabupatenKODE"]; ?>">
                                                                    <?= $kabupaten["kabupatenNAMA"]; ?>
                                                                </option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>




                                                    <div class="form-group">
                                                        <label for="fotoobyekKET">Keterangan</label>
                                                        <textarea class="form-control" id="fotoobyekKET" name="fotoobyekKET" placeholder="Keterangan" rows="6"></textarea>

                                                    </div>



                                                    <div class='form-group'>
                                                        <label for='datepicker'>tanggal ambil</label>

                                                        <input type='text' class='form-control' id='datepicker' name='fotoobyekTGLAMBIL' placeholder='fotoobyekTGLAMBIL'>
                                                        <script></script>

                                                    </div>

                                                    <div class='form-group'>
                                                        <label for='Gambar'>Upload Gambar</label>
                                                        <input type='file' id='Gambar' name='Gambar'>
                                                    </div>

                                                    <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>





                <div class="row">
                    <div class="col-sm">
                        <div class="card">
                            <div id="tablecontainer">
                                <table class="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th scope="col">No</th>
                                            <th scope="col">Kode Foto</th>
                                            <th scope="col">Nama Foto</th>
                                            <th scope="col">Kode Obyek</th>
                                            <th scope="col">Keterangan</th>
                                            <th scope="col">Tanggal Ambil</th>
                                            <th scope="col">Gambar</th>
                                            <th scope="col">action</th>

                                        </tr>
                                    </thead>

                                    <tbody>

                                        <?php $x = $awalData + 1; ?>
                                        <?php foreach ($selectfoto as $foto) : ?>
                                            <tr>

                                                <td><?= $x; ?></td>
                                                <td><?= $foto["fotoobyekKODE"]; ?></td>
                                                <td><?= $foto["fotoobyekNAMA"]; ?></td>
                                                <td><?= $foto["obyekKODE"]; ?></td>
                                                <td><?= $foto["fotoobyekKET"]; ?></td>
                                                <td><?= $foto["fotoobyekTGLAMBIL"]; ?></td>
                                                <td><img src="img/<?= $foto["fotoobyekGAMBAR"]; ?>" alt="" width="50" height="50"> </td>


                                                <td>
                                                    <a href="editfotowisata.php?fotoobyekKODE=<?= $foto["fotoobyekKODE"]; ?>"><button type="submit" name="update" class="btn btn-secondary">Update</button></a> |

                                                    <a href="?fotoobyekKODE=<?= $foto["fotoobyekKODE"]; ?>"> <button type="submit" name="delete" class="btn btn-secondary">Delete</button></a>

                                                </td>



                                            </tr>
                                            <?php $x++; ?>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <!-- pagination -->
                                <nav aria-label="Page navigation example">
                                    <ul class="pagination">

                                        <?php if ($activePage > 1) : ?>
                                            <li class="page-item"><a class="page-link" href="?page=<?= $activePage - 1; ?>">Previous</a></li>
                                        <?php endif; ?>

                                        <?php for ($i = 1; $i <= $jumlahHalaman; $i++) : ?>

                                            <?php if ($i == $activePage) : ?>

                                                <li class="page-item"><a class="page-link" style="color:red" href="?page=<?= $i ?>"><?= $i; ?></a></li>
                                            <?php else : ?>
                                                <li class="page-item"><a class="page-link" href="?page=<?= $i ?>"><?= $i; ?></a></li>


                                            <?php endif; ?>
                                        <?php endfor; ?>

                                        <?php if ($activePage < $jumlahHalaman) : ?>
                                            <li class="page-item"><a class="page-link" href="?page=<?= $activePage + 1; ?>">Next</a></li>
                                        <?php endif; ?>
                                    </ul>
                                </nav>


                            </div>


                        </div>
                    </div>
                </div>






            </section>

        </div>
    </div>
</div>




<?php include "include/_footer.php"; ?>