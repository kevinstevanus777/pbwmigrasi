<?php
include "include/_header.php";

$selectslider = query("SELECT * FROM slider");

// $selectberita = query("SELECT * FROM berita");

if (isset($_POST["submit"])) {


    if (insertslider($_POST) > 0) {
        echo "<script>
    alert('data berhasil ditambahkan!');
    document.location.href = ''
</script>";
    } else {
        echo "<script>
    alert('data tidak berhasil ditambahkan!');
    document.location.href = ''
</script>";
    }
}


if (isset($_GET["sliderKODE"])) {


    if (deletedata($_GET["sliderKODE"], "slider", "sliderKODE") > 0) {
        echo "<script>
    alert('data berhasil didelete!');
    document.location.href = 'slider.php'
</script>";
    } else {
        echo "<script>
        alert('data tidak berhasil didelete!');
        document.location.href = 'slider.php'
    </script>";
    }
}


$dataPerPagination = 5;
$jumlahTotalIndex = count(query("SELECT * FROM berita"));
$jumlahHalaman = ceil($jumlahTotalIndex / $dataPerPagination);
$activePage = (isset($_GET["page"])) ? $_GET["page"] : 1;
$awalData = ($dataPerPagination * $activePage) - $dataPerPagination;
// $selectberita = query("SELECT * FROM berita,kategoriberita,kabupaten
//                         where berita.kategoriberitaKODE = kategoriberita.kategoriberitaKODE 
//                         AND kabupaten.kabupatenKODE = berita.kabupatenKODE
//                         LIMIT $awalData , $dataPerPagination");



$selectberita = query("SELECT * FROM berita,kategoriberita,kabupaten
                        where berita.kategoriberitaKODE = kategoriberita.kategoriberitaKODE 
                        AND kabupaten.kabupatenKODE = berita.kabupatenKODE
                        LIMIT $awalData , $dataPerPagination");


// select * from berita , kategoriberita where berita.kategoriberitaKODE = kategoriberita.kategoriberitaKODE

?>




<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">

            <!-- /# row -->
            <section id="main-content">


                <div class="row">
                    <div class="col-md-auto">
                        <div class="card">
                            <div class="card-body">
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                                    Insert Slider
                                </button>


                                <!-- Modal -->
                                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Input Slider</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">


                                                    <div class='form-group'>
                                                        <label for='sliderKODE'>Kode Slider</label>
                                                        <input type='text' class='form-control' id='sliderKODE' name='sliderKODE' placeholder='Kode Slider'>
                                                    </div>
                                                    <div class='form-group'>
                                                        <label for='sliderJUDUL'>Judul Slider</label>
                                                        <input type='text' class='form-control' id='sliderJUDUL' name='sliderJUDUL' placeholder='Judul Slider'>
                                                    </div>
                                                    <div class='form-group'>
                                                        <label for='sliderKET'>Keterangan Slider</label>
                                                        <input type='text' class='form-control' id='sliderKET' name='sliderKET' placeholder='Keterangan Slider'>
                                                    </div>


                                                    <div class='form-group'>
                                                        <label for='Gambar'>Upload Gambar</label>
                                                        <input type='file' id='Gambar' name='Gambar'>
                                                    </div>

                                                    <div class='form-group'>
                                                        <button type='submit' name='submit' class='btn btn-primary'>Submit</button>
                                                    </div>

                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm">
                        <div class="card">
                            <div id="tablecontainer">
                                <table class="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th scope="col">No</th>
                                            <th scope="col">Kode Slider</th>
                                            <th scope="col">Judul Slider</th>
                                            <th scope="col">Keterangan Slider</th>
                                            <th scope="col">Foto Slider</th>
                                            <th scope="col">action</th>

                                        </tr>
                                    </thead>

                                    <tbody>

                                        <?php $x = $awalData + 1; ?>
                                        <?php foreach ($selectslider as $slider) : ?>
                                            <tr>

                                                <td><?= $x; ?></td>
                                                <td><?= $slider["sliderKODE"]; ?></td>
                                                <td><?= $slider["sliderJUDUL"]; ?></td>
                                                <td><?= $slider["sliderKET"]; ?></td>


                                                <td><img src="img/<?= $slider["sliderFOTO"]; ?>" alt="" width="50" height="50"> </td>


                                                <td>
                                                    <!-- <a href="editisiberita.php?beritaKODE=<?= $slider["beritaKODE"]; ?>"><button type="submit" name="update" class="btn btn-secondary">Update</button></a> | -->
                                                    <a href="?sliderKODE=<?= $slider["sliderKODE"]; ?>"> <button type="submit" name="delete" class="btn btn-secondary">Delete</button></a>

                                                </td>



                                            </tr>
                                            <?php $x++; ?>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <!-- pagination -->
                                <nav aria-label="Page navigation example">
                                    <ul class="pagination">

                                        <?php if ($activePage > 1) : ?>
                                            <li class="page-item"><a class="page-link" href="?page=<?= $activePage - 1; ?>">Previous</a></li>
                                        <?php endif; ?>

                                        <?php for ($i = 1; $i <= $jumlahHalaman; $i++) : ?>

                                            <?php if ($i == $activePage) : ?>

                                                <li class="page-item"><a class="page-link" style="color:red" href="?page=<?= $i ?>"><?= $i; ?></a></li>
                                            <?php else : ?>
                                                <li class="page-item"><a class="page-link" href="?page=<?= $i ?>"><?= $i; ?></a></li>


                                            <?php endif; ?>
                                        <?php endfor; ?>

                                        <?php if ($activePage < $jumlahHalaman) : ?>
                                            <li class="page-item"><a class="page-link" href="?page=<?= $activePage + 1; ?>">Next</a></li>
                                        <?php endif; ?>
                                    </ul>
                                </nav>


                            </div>


                        </div>
                    </div>
                </div>



            </section>

        </div>
    </div>
</div>




<?php include "include/_footer.php"; ?>