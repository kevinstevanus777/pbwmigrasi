create table hotel(
    nomor int primary key auto_increment,
    kecamatanKODE char(4),
    namahotel varchar(50),
    bintanghotel char(1),
    keteranganhotel varchar(255),
    fotohotel varchar(255),
    foreign key (kecamatanKODE) references kecamatan(kecamatanKODE)
);

create table restoran(
    nomor int primary key auto_increment,
    kecamatanKODE char(4),
    namarestoran varchar(50),
    keteranganrestoran varchar(255),
    fotorestoran varchar(255),
    foreign key (kecamatanKODE) references kecamatan(kecamatanKODE)
);

create table provinsi(
    provinsiKODE varchar(10) primary key,
    provinsiNAMA varchar(255),
    kabupatenKODE char(4),
    kecamatanKODE char(4),
    ibukota varchar(255),
    luaswilayah int,
    foto varchar(255),
    foreign key (kabupatenKODE) references kabupaten(kabupatenKODE),
    foreign key (kecamatanKODE) references kecamatan(kecamatanKODE)
);

