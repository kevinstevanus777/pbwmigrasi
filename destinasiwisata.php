<?php
include "include/_header.php";

$selectkecamatan = query("SELECT * FROM kecamatan");

$selectkategoriwisata = query("SELECT * FROM kategoriwisata");
if (isset($_POST["submit"])) {



    if (insertdesdinasiwisata($_POST) > 0) {
        echo "<script>
    alert('data berhasil ditambahkan!');
    document.location.href = ''
</script>";
    } else {
        echo "<script>
    alert('data tidak berhasil ditambahkan!');
    document.location.href = ''
</script>";
    }
}


if (isset($_GET["obyekKODE"])) {


    if (deletedata($_GET["obyekKODE"], "obyekwisata", "obyekKODE") > 0) {
        echo "<script>
    alert('data berhasil didelete!');
    document.location.href = 'destinasiwisata.php'
</script>";
    } else {
        echo "<script>
        alert('data tidak berhasil didelete!');
        document.location.href = 'destinasiwisata.php'
    </script>";
    }
}


$dataPerPagination = 5;
$jumlahTotalIndex = count(query("SELECT * FROM obyekwisata, kategoriwisata,kecamatan
WHERE obyekwisata.kategoriKODE = kategoriwisata.kategoriKODE and obyekwisata.kecamatanKODE = kecamatan.kecamatanKODE"));
$jumlahHalaman = ceil($jumlahTotalIndex / $dataPerPagination);
$activePage = (isset($_GET["page"])) ? $_GET["page"] : 1;
$awalData = ($dataPerPagination * $activePage) - $dataPerPagination;

$selectdestinasiwisata = query("SELECT * FROM obyekwisata, kategoriwisata,kecamatan
WHERE obyekwisata.kategoriKODE = kategoriwisata.kategoriKODE and obyekwisata.kecamatanKODE = kecamatan.kecamatanKODE LIMIT $awalData , $dataPerPagination");

?>



<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">

            <!-- /# row -->
            <section id="main-content">

                <div class="row">
                    <div class="col-md-auto">
                        <div class="card">
                            <div class="card-body">

                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg">Input Destinasi Wisata</button>

                                <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">

                                            <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">

                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-6">

                                                            <div class='form-group'>
                                                                <label for='obyekKODE'>Kode Obyek</label>
                                                                <input type='text' class='form-control' id='obyekKODE' name='obyekKODE' placeholder='Kode Obyek'>
                                                            </div>

                                                            <div class='form-group'>
                                                                <label for='obyekNAMA'>Nama Obyek</label>
                                                                <input type='text' class='form-control' id='obyekNAMA' name='obyekNAMA' placeholder='Nama Obyek'>
                                                            </div>



                                                            <div class='form-group'>
                                                                <label for='kecamatanKODE'>Nama Kecamatan</label>
                                                                <select name="kecamatanKODE" id="kecamatanKODE" class="form-control">

                                                                    <?php foreach ($selectkecamatan as $kecamatan) : ?>

                                                                        <option value="<?= $kecamatan["kecamatanKODE"]; ?>">
                                                                            <?= $kecamatan["kecamatanNAMA"]; ?>
                                                                        </option>
                                                                    <?php endforeach; ?>
                                                                </select>
                                                            </div>


                                                            <div class='form-group'>
                                                                <label for='kategoriKODE'>Nama Kategori Wisata</label>
                                                                <select name="kategoriKODE" id="kategoriKODE" class="form-control">

                                                                    <?php foreach ($selectkategoriwisata as $kategori) : ?>

                                                                        <option value="<?= $kategori["kategoriKODE"]; ?>">
                                                                            <?= $kategori["kategoriNAMA"]; ?>
                                                                        </option>
                                                                    <?php endforeach; ?>
                                                                </select>
                                                            </div>


                                                            <div class='form-group'>
                                                                <label for='obyekALAMAT'>Alamat Obyek</label>
                                                                <input type='text' class='form-control' id='obyekALAMAT' name='obyekALAMAT' placeholder='Alamat Obyek'>
                                                            </div>


                                                            <div class='form-group'>
                                                                <label for='obyekDERAJAT_S'>Obyek Derajat S</label>
                                                                <input type='text' class='form-control' id='obyekDERAJAT_S' name='obyekDERAJAT_S' placeholder='Obyek Derajat S'>
                                                            </div>


                                                            <div class='form-group'>
                                                                <label for='obyekMENIT_S'>Obyek Menit S</label>
                                                                <input type='text' class='form-control' id='obyekMENIT_S' name='obyekMENIT_S' placeholder='Obyek Menit S'>
                                                            </div>

                                                            <div class='form-group'>
                                                                <label for='obyekDETIK_S'>Obyek Detik S</label>
                                                                <input type='text' class='form-control' id='obyekDETIK_S' name='obyekDETIK_S' placeholder='Obyek Detik S'>
                                                            </div>




                                                        </div>



                                                        <div class="col-md-6">
                                                            <div class='form-group'>
                                                                <label for='obyekLATITUDE'>Obyek Latitude</label>
                                                                <input type='text' class='form-control' id='obyekLATITUDE' name='obyekLATITUDE' placeholder='Obyek Latitude'>
                                                            </div>
                                                            <div class='form-group'>
                                                                <label for='obyekDERAJAT_E'>Obyek Derajat E</label>
                                                                <input type='text' class='form-control' id='obyekDERAJAT_E' name='obyekDERAJAT_E' placeholder='Obyek Derajat E'>
                                                            </div>
                                                            <div class='form-group'>
                                                                <label for='obyekMENIT_E'>Obyek Menit E</label>
                                                                <input type='text' class='form-control' id='obyekMENIT_E' name='obyekMENIT_E' placeholder='Obyek Menit E'>
                                                            </div>

                                                            <div class='form-group'>
                                                                <label for='obyekDETIK_E'>Obyek Detik E</label>
                                                                <input type='text' class='form-control' id='obyekDETIK_E' name='obyekDETIK_E' placeholder='Obyek Detik E'>
                                                            </div>

                                                            <div class='form-group'>
                                                                <label for='obyekLONGITUDE'>Obyek Longitude</label>
                                                                <input type='text' class='form-control' id='obyekLONGITUDE' name='obyekLONGITUDE' placeholder='Obyek Longitude'>
                                                            </div>

                                                            <div class='form-group'>
                                                                <label for='obyekKETINGGIAN'>Obyek Ketinggian</label>
                                                                <input type='text' class='form-control' id='obyekKETINGGIAN' name='obyekKETINGGIAN' placeholder='Obyek Ketinggian'>
                                                            </div>


                                                            <div class='form-group'>
                                                                <label for='obyekDEFINISI'>Obyek Definisi</label>
                                                                <input type='text' class='form-control' id='obyekDEFINISI' name='obyekDEFINISI' placeholder='Obyek Definisi'>
                                                            </div>

                                                            <div class='form-group'>
                                                                <label for='obyekKETERANGAN'>Keterangan</label>
                                                                <input type='text' class='form-control' id='obyekKETERANGAN' name='obyekKETERANGAN' placeholder='Keterangan'>
                                                            </div>


                                                            <div class='form-group'>
                                                                <label for='Gambar'>Upload Gambar</label>
                                                                <input type='file' id='Gambar' name='Gambar'>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                                                </div>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="row">
                    <div class="col-sm">
                        <div class="card">
                            <div id="tablecontainer">


                                <table class="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th scope="col">No</th>
                                            <th scope="col">Kode Obyek</th>
                                            <th scope="col">Nama Obyek</th>
                                            <th scope="col">Nama Kecamatan</th>
                                            <th scope="col">Nama Kategori</th>
                                            <th scope="col">Alamat Obyek</th>
                                            <!-- <th scope="col">DERAJAT_S</th>
                                            <th scope="col">MENIT_S</th>
                                            <th scope="col">DETIK_S</th>
                                            <th scope="col">LATITUDE</th>
                                            <th scope="col">DERAJAT_E</th>
                                            <th scope="col">MENIT_E</th>
                                            <th scope="col">DETIK_E</th>
                                            <th scope="col">LONGITUDE</th>
                                            <th scope="col">KETINGGIAN</th> -->
                                            <th scope="col">DEFINISI</th>
                                            <th scope="col">KETERANGAN</th>
                                            <th scope="col">foto</th>
                                            <th scope="col">action</th>

                                        </tr>
                                    </thead>


                                    <tbody>

                                        <?php $x = $awalData + 1; ?>
                                        <?php foreach ($selectdestinasiwisata as $destinasi) : ?>
                                            <tr>

                                                <td><?= $x; ?></td>
                                                <td><?= $destinasi["obyekKODE"]; ?></td>
                                                <td><?= $destinasi["obyekNAMA"]; ?></td>
                                                <td><?= $destinasi["kecamatanNAMA"]; ?></td>
                                                <td><?= $destinasi["kategoriNAMA"]; ?></td>
                                                <td><?= $destinasi["obyekALAMAT"]; ?></td>
                                                <!-- <td><?= $destinasi["obyekDERAJAT_S"]; ?></td>
                                                <td><?= $destinasi["obyekMENIT_S"]; ?></td>
                                                <td><?= $destinasi["obyekDETIK_S"]; ?></td>
                                                <td><?= $destinasi["obyekLATITUDE"]; ?></td>
                                                <td><?= $destinasi["obyekDERAJAT_E"]; ?></td>
                                                <td><?= $destinasi["obyekMENIT_E"]; ?></td>
                                                <td><?= $destinasi["obyekDETIK_E"]; ?></td>
                                                <td><?= $destinasi["obyekLONGITUDE"]; ?></td>
                                                <td><?= $destinasi["obyekKETINGGIAN"]; ?></td> -->
                                                <td><?= $destinasi["obyekDEFINISI"]; ?></td>
                                                <td><?= $destinasi["obyekKETERANGAN"]; ?></td>

                                                <td><img src="img/<?= $destinasi["obyekFOTO"]; ?>" alt="" width="50" height="50"> </td>


                                                <td>
                                                    <a href="editdestinasiwisata.php?obyekKODE=<?= $destinasi["obyekKODE"]; ?>"><button type="submit" name="update" class="btn btn-secondary">Update</button></a> |
                                                    <a href="?obyekKODE=<?= $destinasi["obyekKODE"]; ?>"> <button type="submit" name="delete" class="btn btn-secondary">Delete</button></a>


                                                </td>



                                            </tr>
                                            <?php $x++; ?>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>

                                <!-- pagination -->
                                <nav aria-label="Page navigation example">
                                    <ul class="pagination">

                                        <?php if ($activePage > 1) : ?>
                                            <li class="page-item"><a class="page-link" href="?page=<?= $activePage - 1; ?>">Previous</a></li>
                                        <?php endif; ?>

                                        <?php for ($i = 1; $i <= $jumlahHalaman; $i++) : ?>

                                            <?php if ($i == $activePage) : ?>

                                                <li class="page-item"><a class="page-link" style="color:red" href="?page=<?= $i ?>"><?= $i; ?></a></li>
                                            <?php else : ?>
                                                <li class="page-item"><a class="page-link" href="?page=<?= $i ?>"><?= $i; ?></a></li>


                                            <?php endif; ?>
                                        <?php endfor; ?>

                                        <?php if ($activePage < $jumlahHalaman) : ?>
                                            <li class="page-item"><a class="page-link" href="?page=<?= $activePage + 1; ?>">Next</a></li>
                                        <?php endif; ?>
                                    </ul>
                                </nav>

                            </div>


                        </div>
                    </div>
                </div>





            </section>

        </div>
    </div>
</div>




<?php include "include/_footer.php"; ?>