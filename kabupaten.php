<?php

include "include/_header.php";


if (isset($_POST["submit"])) {



    if (insertkabupaten($_POST) > 0) {
        echo "<script>
    alert('data berhasil ditambahkan!');
    document.location.href = ''
</script>";
    } else {
        echo "<script>
    alert('data tidak berhasil ditambahkan!');
    document.location.href = ''
</script>";
    }
}


if (isset($_GET["kabupatenKODE"])) {


    if (deletedata($_GET["kabupatenKODE"], "kabupaten", "kabupatenKODE") > 0) {
        echo "<script>
    alert('data berhasil didelete!');
    document.location.href = 'kabupaten.php'
    </script>";
    } else {
        echo "<script>
        alert('data tidak berhasil didelete!');
        document.location.href = 'kabupaten.php'
    </script>";
    }
}


$dataPerPagination = 5;
$jumlahTotalIndex = count(query("SELECT * FROM kabupaten"));
$jumlahHalaman = ceil($jumlahTotalIndex / $dataPerPagination);
$activePage = (isset($_GET["page"])) ? $_GET["page"] : 1;
$awalData = ($dataPerPagination * $activePage) - $dataPerPagination;
// $selectkategoriberita = query("SELECT * FROM kategoriberita LIMIT $awalData , $dataPerPagination");
$selectkabupaten = query("SELECT * FROM kabupaten LIMIT $awalData , $dataPerPagination");

// $selectkategoriwisata = query("SELECT * FROM kategoriwisata LIMIT $awalData , $dataPerPagination");


?>




<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">

            <!-- /# row -->
            <section id="main-content">

                <div class="row">
                    <div class="col-md-auto">
                        <div class="card">
                            <div class="card-body">
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                                    Insert Kabupaten
                                </button>


                                <!-- Modal -->
                                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Input Kabupaten</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
                                                    <div class="form-group">
                                                        <label for="KodeKabupaten">Kode Kabupaten</label>
                                                        <input type="text" class="form-control" id="KodeKabupaten" name="KodeKabupaten" aria-describedby="emailHelp" placeholder="Kode Kabupaten">
                                                        <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="NamaKabupaten">Nama Kabupaten</label>
                                                        <input type="text" class="form-control" id="NamaKabupaten" name="NamaKabupaten" placeholder="Nama Kabupaten">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="AlamatKabupaten">Alamat Kabupaten</label>
                                                        <input type="text" class="form-control" id="AlamatKabupaten" name="AlamatKabupaten" placeholder="Alamat Kabupaten">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="KeteranganKabupaten">Keterangan Kabupaten</label>
                                                        <textarea class="form-control" id="KeteranganKabupaten" name="KeteranganKabupaten" placeholder="Keterangan" rows="6"></textarea>

                                                    </div>


                                                    <div class='form-group'>
                                                        <label for='Gambar'>Upload Gambar</label>
                                                        <input type='file' id='Gambar' name='Gambar'>
                                                    </div>


                                                    <div class='form-group'>
                                                        <label for='kabupatenFOTOICONKET'>Keterangan Foto</label>
                                                        <input type='text' class='form-control' id='kabupatenFOTOICONKET' name='kabupatenFOTOICONKET' placeholder='Keterangan Foto'>
                                                    </div>

                                                    <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm">
                        <div class="card">


                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Kode Kabupaten</th>
                                        <th scope="col">Nama Kabupaten</th>
                                        <th scope="col">Alamat Kabupaten</th>
                                        <th scope="col">Keterangan Kabupaten</th>
                                        <th scope="col">Foto Kabupaten</th>
                                        <th scope="col">Foto Icon Ket Kabupaten</th>
                                        <th scope="col">Actions</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php $i = 1; ?>
                                    <?php foreach ($selectkabupaten as $kabupaten) : ?>
                                        <tr>

                                            <td><?= $i; ?></td>
                                            <td><?= $kabupaten["kabupatenKODE"]; ?></td>
                                            <td><?= $kabupaten["kabupatenNAMA"]; ?></td>
                                            <td><?= $kabupaten["kabupatenALAMAT"]; ?></td>

                                            <td><?= $kabupaten["kabupatenKET"]; ?></td>
                                            <td><img src="img/<?= $kabupaten["kabupatenFOTOICON"]; ?>" alt="" width="50" height="50"> </td>

                                            <!-- <td><?= $kabupaten["kabupatenFOTOICON"]; ?></td> -->
                                            <td><?= $kabupaten["kabupatenFOTOICONKET"]; ?></td>
                                            <td>

                                                <a href="editkabupaten.php?kabupatenKODE=<?= $kabupaten["kabupatenKODE"]; ?>"> <button type="submit" name="update" class="btn btn-secondary">Update</button></a>|
                                                <a href="?kabupatenKODE=<?= $kabupaten["kabupatenKODE"]; ?>"> <button type="submit" name="delete" class="btn btn-secondary">Delete</button></a>

                                            </td>

                                        </tr>
                                        <?php $i++; ?>
                                    <?php endforeach; ?>

                                </tbody>
                            </table>
                            <!-- pagination -->
                            <nav aria-label="Page navigation example">
                                <ul class="pagination">

                                    <?php if ($activePage > 1) : ?>
                                        <li class="page-item"><a class="page-link" href="?page=<?= $activePage - 1; ?>">Previous</a></li>
                                    <?php endif; ?>

                                    <?php for ($i = 1; $i <= $jumlahHalaman; $i++) : ?>

                                        <?php if ($i == $activePage) : ?>

                                            <li class="page-item"><a class="page-link" style="color:red" href="?page=<?= $i ?>"><?= $i; ?></a></li>
                                        <?php else : ?>
                                            <li class="page-item"><a class="page-link" href="?page=<?= $i ?>"><?= $i; ?></a></li>


                                        <?php endif; ?>
                                    <?php endfor; ?>

                                    <?php if ($activePage < $jumlahHalaman) : ?>
                                        <li class="page-item"><a class="page-link" href="?page=<?= $activePage + 1; ?>">Next</a></li>
                                    <?php endif; ?>
                                </ul>
                            </nav>

                        </div>
                    </div>
                </div>


            </section>
        </div>
    </div>
</div>

<?php
include "include/_footer.php"; ?>