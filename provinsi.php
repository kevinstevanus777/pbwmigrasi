<?php
include "include/_header.php";

$selectkecamatan = query("SELECT * FROM kecamatan");
$selectkabupaten = query("SELECT * FROM kabupaten");

if (isset($_POST["submit"])) {


    if (insertprovinsi($_POST) > 0) {
        echo "<script>
    alert('data berhasil ditambahkan!');
    document.location.href = ''
</script>";
    } else {
        echo "<script>
    alert('data tidak berhasil ditambahkan!');
    document.location.href = ''
</script>";
    }
}


if (isset($_GET["provinsiKODE"])) {


    if (deletedata($_GET["provinsiKODE"], "provinsi", "provinsiKODE") > 0) {
        echo "<script>
    alert('data berhasil didelete!');
    document.location.href = 'provinsi.php'
</script>";
    } else {
        echo "<script>
        alert('data tidak berhasil didelete!');
        document.location.href = 'provinsi.php'
    </script>";
    }
}


$dataPerPagination = 5;
$jumlahTotalIndex = count(query("SELECT * FROM provinsi,kecamatan,kabupaten WHERE provinsi.kabupatenKODE = kabupaten.kabupatenKODE AND provinsi.kecamatanKODE = kecamatan.kecamatanKODE"));
$jumlahHalaman = ceil($jumlahTotalIndex / $dataPerPagination);
$activePage = (isset($_GET["page"])) ? $_GET["page"] : 1;
$awalData = ($dataPerPagination * $activePage) - $dataPerPagination;
$selectprovinsi = query("SELECT *
FROM provinsi,kecamatan,kabupaten
WHERE provinsi.kabupatenKODE = kabupaten.kabupatenKODE AND provinsi.kecamatanKODE = kecamatan.kecamatanKODE
LIMIT $awalData , $dataPerPagination");

?>



<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">

            <!-- /# row -->
            <section id="main-content">


                <div class="row">
                    <div class="col-md-auto">
                        <div class="card">
                            <div class="card-body">
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                                    Insert Provinsi
                                </button>


                                <!-- Modal -->
                                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Input Kabupaten</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">


                                                    <div class='form-group'>
                                                        <label for='provinsiKODE'>Kode Provinsi</label>
                                                        <input type='text' class='form-control' id='provinsiKODE' name='provinsiKODE' placeholder='Kode Provinsi'>
                                                    </div>

                                                    <div class='form-group'>
                                                        <label for='provinsiNAMA'>Nama Provinsi</label>
                                                        <input type='text' class='form-control' id='provinsiNAMA' name='provinsiNAMA' placeholder='Nama Provinsi'>
                                                    </div>

                                                    <div class='form-group'>
                                                        <label for='kabupatenKODE'>Nama Kabupaten</label>
                                                        <select name="kabupatenKODE" id="kabupatenKODE" class="form-control">

                                                            <?php foreach ($selectkabupaten as $kabupaten) : ?>

                                                                <option value="<?= $kabupaten["kabupatenKODE"]; ?>">
                                                                    <?= $kabupaten["kabupatenNAMA"]; ?>
                                                                </option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>




                                                    <div class='form-group'>
                                                        <label for='kecamatanKODE'>Nama Kecamatan</label>
                                                        <select name="kecamatanKODE" id="kecamatanKODE" class="form-control">

                                                            <?php foreach ($selectkecamatan as $kecamatan) : ?>

                                                                <option value="<?= $kecamatan["kecamatanKODE"]; ?>">
                                                                    <?= $kecamatan["kecamatanNAMA"]; ?>
                                                                </option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>


                                                    <div class='form-group'>
                                                        <label for='ibukota'>ibukota</label>
                                                        <input type='text' class='form-control' id='ibukota' name='ibukota' placeholder='ibukota'>
                                                    </div>

                                                    <div class='form-group'>
                                                        <label for='luaswilayah'>Luas Wilayah</label>
                                                        <input type='text' class='form-control' id='luaswilayah' name='luaswilayah' placeholder='Luas Wilayah'>
                                                    </div>

                                                    <div class='form-group'>
                                                        <label for='Gambar'>Upload Gambar</label>
                                                        <input type='file' id='Gambar' name='Gambar'>
                                                    </div>

                                                    <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-sm">
                        <div class="card">
                            <div id="tablecontainer">
                                <table class="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th scope="col">No</th>
                                            <th scope="col">Kode Provinsi</th>
                                            <th scope="col">Nama Provinsi</th>
                                            <th scope="col">Nama Kabupaten</th>
                                            <th scope="col">Nama Kecamatan</th>
                                            <th scope="col">Ibukota</th>
                                            <th scope="col">Luas Wilayah</th>
                                            <th scope="col">Foto</th>
                                            <th scope="col">Action</th>

                                        </tr>
                                    </thead>

                                    <tbody>

                                        <?php $x = $awalData + 1; ?>
                                        <?php foreach ($selectprovinsi as $provinsi) : ?>
                                            <tr>

                                                <td><?= $x; ?></td>
                                                <td><?= $provinsi["provinsiKODE"]; ?></td>
                                                <td><?= $provinsi["provinsiNAMA"]; ?></td>
                                                <td><?= $provinsi["kabupatenNAMA"]; ?></td>
                                                <td><?= $provinsi["kecamatanNAMA"]; ?></td>
                                                <td><?= $provinsi["ibukota"]; ?></td>
                                                <td><?= $provinsi["luaswilayah"]; ?></td>
                                                <td><img src="img/<?= $provinsi["foto"]; ?>" alt="" width="50" height="50"> </td>

                                                <td>
                                                    <a href="editprovinsi.php?provinsiKODE=<?= $provinsi["provinsiKODE"]; ?>"><button type="submit" name="update" class="btn btn-secondary">Update</button></a>
                                                    <a href="?provinsiKODE=<?= $provinsi["provinsiKODE"]; ?>"> <button type="submit" name="delete" class="btn btn-secondary">Delete</button></a>
                                                </td>



                                            </tr>
                                            <?php $x++; ?>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <!-- pagination -->
                                <nav aria-label="Page navigation example">
                                    <ul class="pagination">

                                        <?php if ($activePage > 1) : ?>
                                            <li class="page-item"><a class="page-link" href="?page=<?= $activePage - 1; ?>">Previous</a></li>
                                        <?php endif; ?>

                                        <?php for ($i = 1; $i <= $jumlahHalaman; $i++) : ?>

                                            <?php if ($i == $activePage) : ?>

                                                <li class="page-item"><a class="page-link" style="color:red" href="?page=<?= $i ?>"><?= $i; ?></a></li>
                                            <?php else : ?>
                                                <li class="page-item"><a class="page-link" href="?page=<?= $i ?>"><?= $i; ?></a></li>


                                            <?php endif; ?>
                                        <?php endfor; ?>

                                        <?php if ($activePage < $jumlahHalaman) : ?>
                                            <li class="page-item"><a class="page-link" href="?page=<?= $activePage + 1; ?>">Next</a></li>
                                        <?php endif; ?>
                                    </ul>
                                </nav>


                            </div>


                        </div>
                    </div>
                </div>





            </section>

        </div>
    </div>
</div>




<?php include "include/_footer.php"; ?>