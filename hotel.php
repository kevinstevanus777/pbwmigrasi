<?php
include "include/_header.php";

$selectkecamatan = query("SELECT * FROM kecamatan");


if (isset($_POST["submit"])) {


    if (inserthotel($_POST) > 0) {
        echo "<script>
    alert('data berhasil ditambahkan!');
    document.location.href = ''
</script>";
    } else {
        echo "<script>
    alert('data tidak berhasil ditambahkan!');
    document.location.href = ''
</script>";
    }
}


if (isset($_GET["nomor"])) {


    if (deletedata($_GET["nomor"], "hotel", "nomor") > 0) {
        echo "<script>
    alert('data berhasil didelete!');
    document.location.href = 'hotel.php'
</script>";
    } else {
        echo "<script>
        alert('data tidak berhasil didelete!');
        document.location.href = 'hotel.php'
    </script>";
    }
}

$dataPerPagination = 5;
$jumlahTotalIndex = count(query("SELECT * FROM hotel,kecamatan WHERE hotel.kecamatanKODE = kecamatan.kecamatanKODE"));
$jumlahHalaman = ceil($jumlahTotalIndex / $dataPerPagination);
$activePage = (isset($_GET["page"])) ? $_GET["page"] : 1;
$awalData = ($dataPerPagination * $activePage) - $dataPerPagination;


$selecthotel = query("SELECT * FROM hotel,kecamatan WHERE hotel.kecamatanKODE = kecamatan.kecamatanKODE LIMIT $awalData , $dataPerPagination");



?>



<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">

            <!-- /# row -->
            <section id="main-content">


                <div class="row">
                    <div class="col-md-auto">
                        <div class="card">
                            <div class="card-body">
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                                    Insert Hotel
                                </button>



                                <!-- Modal -->
                                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Input Hotel</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">



                                                    <div class='form-group'>
                                                        <label for='kecamatanKODE'>Nama Kecamatan</label>
                                                        <select name="kecamatanKODE" id="kecamatanKODE" class="form-control">

                                                            <?php foreach ($selectkecamatan as $kecamatan) : ?>

                                                                <option value="<?= $kecamatan["kecamatanKODE"]; ?>">
                                                                    <?= $kecamatan["kecamatanNAMA"]; ?>
                                                                </option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>


                                                    <div class='form-group'>
                                                        <label for='namahotel'>Nama Hotel</label>
                                                        <input type='text' class='form-control' id='namahotel' name='namahotel' placeholder='Nama Hotel'>
                                                    </div>



                                                    <div class='form-group'>
                                                        <label for='bintanghotel'>Bintang Hotel</label>
                                                        <input type='text' class='form-control' id='bintanghotel' name='bintanghotel' placeholder='Bintang Hotel'>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="keteranganhotel">Keterangan Hotel</label>
                                                        <textarea class="form-control" id="keteranganhotel" name="keteranganhotel" placeholder="Keterangan" rows="6"></textarea>

                                                    </div>


                                                    <div class='form-group'>
                                                        <label for='Gambar'>Upload Gambar</label>
                                                        <input type='file' id='Gambar' name='Gambar'>
                                                    </div>




                                                    <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-sm">
                        <div class="card">
                            <div id="tablecontainer">
                                <table class="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th scope="col">No</th>
                                            <th scope="col">Kecamatan</th>
                                            <th scope="col">Nama Hotel</th>
                                            <th scope="col">Bintang Hotel</th>
                                            <th scope="col">Keterangan</th>
                                            <th scope="col">foto icon</th>
                                            <th scope="col">action</th>

                                        </tr>
                                    </thead>

                                    <tbody>

                                        <?php $x = $awalData + 1; ?>
                                        <?php foreach ($selecthotel as $hotel) : ?>
                                            <tr>

                                                <td><?= $x; ?></td>
                                                <td><?= $hotel["kecamatanNAMA"]; ?></td>
                                                <td><?= $hotel["namahotel"]; ?></td>
                                                <td><?= $hotel["bintanghotel"]; ?></td>
                                                <td><?= $hotel["keteranganhotel"]; ?></td>

                                                <td><img src="img/<?= $hotel["fotohotel"]; ?>" alt="" width="50" height="50"> </td>


                                                <td>
                                                    <a href="edithotel.php?nomor=<?= $hotel["nomor"]; ?>"><button type="submit" name="update" class="btn btn-secondary">Update</button></a> |
                                                    <a href="?nomor=<?= $hotel["nomor"]; ?>"> <button type="submit" name="delete" class="btn btn-secondary">Delete</button></a>


                                                </td>



                                            </tr>
                                            <?php $x++; ?>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <!-- pagination -->
                                <nav aria-label="Page navigation example">
                                    <ul class="pagination">

                                        <?php if ($activePage > 1) : ?>
                                            <li class="page-item"><a class="page-link" href="?page=<?= $activePage - 1; ?>">Previous</a></li>
                                        <?php endif; ?>

                                        <?php for ($i = 1; $i <= $jumlahHalaman; $i++) : ?>

                                            <?php if ($i == $activePage) : ?>

                                                <li class="page-item"><a class="page-link" style="color:red" href="?page=<?= $i ?>"><?= $i; ?></a></li>
                                            <?php else : ?>
                                                <li class="page-item"><a class="page-link" href="?page=<?= $i ?>"><?= $i; ?></a></li>


                                            <?php endif; ?>
                                        <?php endfor; ?>

                                        <?php if ($activePage < $jumlahHalaman) : ?>
                                            <li class="page-item"><a class="page-link" href="?page=<?= $activePage + 1; ?>">Next</a></li>
                                        <?php endif; ?>
                                    </ul>
                                </nav>


                            </div>


                        </div>
                    </div>
                </div>



            </section>

        </div>
    </div>
</div>




<?php include "include/_footer.php"; ?>