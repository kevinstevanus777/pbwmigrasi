<?php
include "include/_header.php";



if (isset($_POST["submit"])) {
    if (insertkategoriwisata($_POST) > 0) {
        echo "<script>
        alert('data berhasil ditambahkan!');
        document.location.href= ''
        </script>";
    } else {
        echo "<script>
        alert('data berhasil ditambahkan!');
        document.location.href= ''
        </script>";
    }
}
if (isset($_GET["kategoriKODE"])) {


    if (deletedata($_GET["kategoriKODE"], "kategoriwisata", "kategoriKODE") > 0) {
        echo "<script>
    alert('data berhasil didelete!');
    document.location.href = 'kategoriwisata.php'
</script>";
    } else {
        echo "<script>
        alert('data tidak berhasil didelete!');
        document.location.href = 'kategoriwisata.php'
    </script>";
    }
}


$dataPerPagination = 5;
$jumlahTotalIndex = count(query("SELECT * FROM kategoriwisata"));
$jumlahHalaman = ceil($jumlahTotalIndex / $dataPerPagination);
$activePage = (isset($_GET["page"])) ? $_GET["page"] : 1;
$awalData = ($dataPerPagination * $activePage) - $dataPerPagination;
// $selectkategoriberita = query("SELECT * FROM kategoriberita LIMIT $awalData , $dataPerPagination");

$selectkategoriwisata = query("SELECT * FROM kategoriwisata LIMIT $awalData , $dataPerPagination");

?>



<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">

            <!-- /# row -->
            <section id="main-content">


                <div class="row">
                    <div class="col-md-auto">
                        <div class="card">
                            <div class="card-body">
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                                    Insert Kategori Wisata
                                </button>


                                <!-- Modal -->
                                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Input Kabupaten</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form class="form-horizontal" action="" method="post">

                                                    <div class='form-group'>
                                                        <label for='kategoriKODE'>Kode Kategori</label>
                                                        <input type='text' class='form-control' id='kategoriKODE' name='kategoriKODE' placeholder='Kode Kategori'>
                                                    </div>

                                                    <div class='form-group'>
                                                        <label for='kategoriNAMA'>Nama Kategori</label>
                                                        <input type='text' class='form-control' id='kategoriNAMA' name='kategoriNAMA' placeholder='Nama Kategori'>
                                                    </div>



                                                    <div class="form-group">
                                                        <label for="kategoriKET">Keterangan Kategori</label>
                                                        <textarea class="form-control" id="kategoriKET" name="kategoriKET" placeholder="Keterangan" rows="6"></textarea>

                                                    </div>

                                                    <div class='form-group'>
                                                        <label for='kategoriREFERENCE'>Referensi Kategori</label>
                                                        <input type='text' class='form-control' id='kategoriREFERENCE' name='kategoriREFERENCE' placeholder='Referensi Kategori'>
                                                    </div>



                                                    <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-sm">
                        <div class="card">
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Kode Kategori</th>
                                        <th scope="col">Nama Kategori</th>
                                        <th scope="col">Keterangan Kategori</th>
                                        <th scope="col">Referensi Kategori</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>

                                <tbody>

                                    <?php $x = 1; ?>
                                    <?php foreach ($selectkategoriwisata as $data) : ?>
                                        <tr>

                                            <td><?= $x; ?></td>
                                            <td><?= $data["kategoriKODE"]; ?></td>
                                            <td><?= $data["kategoriNAMA"]; ?></td>
                                            <td><?= $data["kategoriKET"]; ?></td>
                                            <td><?= $data["kategoriREFERENCE"]; ?></td>
                                            <td><a href="editkategoriwisata.php?kategoriKODE=<?= $data["kategoriKODE"]; ?>"><button type="submit" name="update" class="btn btn-secondary">Update</button></a>
                                                | <a href="?kategoriKODE=<?= $data["kategoriKODE"]; ?>"> <button type="submit" name="delete" class="btn btn-secondary">Delete</button></a>

                                            </td>

                                        </tr>
                                        <?php $x++; ?>
                                    <?php endforeach; ?>

                                </tbody>
                            </table>


                            <!-- pagination -->
                            <nav aria-label="Page navigation example">
                                <ul class="pagination">

                                    <?php if ($activePage > 1) : ?>
                                        <li class="page-item"><a class="page-link" href="?page=<?= $activePage - 1; ?>">Previous</a></li>
                                    <?php endif; ?>

                                    <?php for ($i = 1; $i <= $jumlahHalaman; $i++) : ?>

                                        <?php if ($i == $activePage) : ?>

                                            <li class="page-item"><a class="page-link" style="color:red" href="?page=<?= $i ?>"><?= $i; ?></a></li>
                                        <?php else : ?>
                                            <li class="page-item"><a class="page-link" href="?page=<?= $i ?>"><?= $i; ?></a></li>


                                        <?php endif; ?>
                                    <?php endfor; ?>

                                    <?php if ($activePage < $jumlahHalaman) : ?>
                                        <li class="page-item"><a class="page-link" href="?page=<?= $activePage + 1; ?>">Next</a></li>
                                    <?php endif; ?>
                                </ul>
                            </nav>

                        </div>
                    </div>
                </div>

            </section>

        </div>
    </div>
</div>




<?php include "include/_footer.php"; ?>