<?php
include "include/_header.php";

$selectkecamatan = query("SELECT * FROM kecamatan");
$selectkabupaten = query("SELECT * FROM kabupaten");

if (isset($_POST["submit"])) {


    if (editprovinsi($_POST) > 0) {
        echo "<script>
    alert('data berhasil ditambahkan!');
    document.location.href = 'provinsi.php'
</script>";
    } else {
        echo "<script>
    alert('data tidak berhasil ditambahkan!');
    document.location.href = ''
</script>";
    }
}

$id = $_GET["provinsiKODE"];

$selectprovinsi = query("SELECT * FROM provinsi where provinsikode = '$id'")[0];


?>



<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">

            <!-- /# row -->
            <section id="main-content">


                <div class="row">
                    <div class="col-md-auto">
                        <div class="card">


                            <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">


                                <div class='form-group'>
                                    <label for='provinsiKODE'>Kode Provinsi</label>
                                    <input type='text' class='form-control' id='provinsiKODE' name='provinsiKODE' placeholder='Kode Provinsi' value="<?= $selectprovinsi["provinsiKODE"]; ?>">
                                </div>

                                <div class='form-group'>
                                    <label for='provinsiNAMA'>Nama Provinsi</label>
                                    <input type='text' class='form-control' id='provinsiNAMA' name='provinsiNAMA' placeholder='Nama Provinsi' value="<?= $selectprovinsi["provinsiNAMA"]; ?>">
                                </div>

                                <div class='form-group'>
                                    <label for='kabupatenKODE'>Nama Kabupaten</label>
                                    <select name="kabupatenKODE" id="kabupatenKODE" class="form-control">

                                        <?php foreach ($selectkabupaten as $kabupaten) : ?>

                                            <option value="<?php echo $kabupaten["kabupatenKODE"]; ?>" <?php if ($kabupaten["kabupatenKODE"] === $selectprovinsi["kabupatenKODE"]) {
                                                                                                                echo "selected";
                                                                                                            } ?>>
                                                <?php echo $kabupaten["kabupatenNAMA"]; ?>
                                            </option>
                                        <?php endforeach; ?>


                                    </select>
                                </div>

                                <div class='form-group'>
                                    <label for='kecamatanKODE'>Nama Kecamatan</label>
                                    <select name="kecamatanKODE" id="kecamatanKODE" class="form-control">

                                        <?php foreach ($selectkecamatan as $kecamatan) : ?>

                                            <option value="<?php echo $kecamatan["kecamatanKODE"]; ?>" <?php if ($kecamatan["kecamatanKODE"] === $selectprovinsi["kecamatanKODE"]) {
                                                                                                                echo "selected";
                                                                                                            } ?>>
                                                <?php echo $kecamatan["kecamatanNAMA"]; ?>
                                            </option>
                                        <?php endforeach; ?>

                                        <!-- <?php foreach ($selectkecamatan as $kecamatan) : ?>

                                            <option value="<?= $kecamatan["kecamatanKODE"]; ?>">
                                                <?= $kecamatan["kecamatanNAMA"]; ?>
                                            </option>
                                        <?php endforeach; ?> -->
                                    </select>
                                </div>


                                <div class='form-group'>
                                    <label for='ibukota'>ibukota</label>
                                    <input type='text' class='form-control' id='ibukota' name='ibukota' placeholder='ibukota' value="<?= $selectprovinsi["ibukota"]; ?>">
                                </div>

                                <div class='form-group'>
                                    <label for='luaswilayah'>Luas Wilayah</label>
                                    <input type='text' class='form-control' id='luaswilayah' name='luaswilayah' placeholder='Luas Wilayah' value="<?= $selectprovinsi["luaswilayah"]; ?>">
                                </div>

                                <div class='form-group'>
                                    <label for='Gambar'>Upload Gambar</label>
                                    <img src="img/<?= $selectprovinsi["foto"]; ?>" alt="" width="50" height="50">
                                    <input type='file' id='Gambar' name='Gambar'>
                                </div>



                                <input type="hidden" name="GambarLama" value="<?= $selectprovinsi["foto"]; ?>">



                                <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                            </form>



                        </div>
                    </div>
                </div>







            </section>

        </div>
    </div>
</div>




<?php include "include/_footer.php"; ?>