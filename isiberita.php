<?php
include "include/_header.php";

$selectkategoriberita = query("SELECT * FROM kategoriberita");

$selectkabupaten = query("SELECT * FROM kabupaten");

// $selectberita = query("SELECT * FROM berita");

if (isset($_POST["submit"])) {


    if (insertisiberita($_POST) > 0) {
        echo "<script>
    alert('data berhasil ditambahkan!');
    document.location.href = ''
</script>";
    } else {
        echo "<script>
    alert('data tidak berhasil ditambahkan!');
    document.location.href = ''
</script>";
    }
}


if (isset($_GET["beritaKODE"])) {


    if (deletedata($_GET["beritaKODE"], "berita", "beritaKODE") > 0) {
        echo "<script>
    alert('data berhasil didelete!');
    document.location.href = 'isiberita.php'
</script>";
    } else {
        echo "<script>
        alert('data tidak berhasil didelete!');
        document.location.href = 'isiberita.php'
    </script>";
    }
}


$dataPerPagination = 5;
$jumlahTotalIndex = count(query("SELECT * FROM berita"));
$jumlahHalaman = ceil($jumlahTotalIndex / $dataPerPagination);
$activePage = (isset($_GET["page"])) ? $_GET["page"] : 1;
$awalData = ($dataPerPagination * $activePage) - $dataPerPagination;
// $selectberita = query("SELECT * FROM berita,kategoriberita,kabupaten
//                         where berita.kategoriberitaKODE = kategoriberita.kategoriberitaKODE 
//                         AND kabupaten.kabupatenKODE = berita.kabupatenKODE
//                         LIMIT $awalData , $dataPerPagination");



$selectberita = query("SELECT * FROM berita,kategoriberita,kabupaten
                        where berita.kategoriberitaKODE = kategoriberita.kategoriberitaKODE 
                        AND kabupaten.kabupatenKODE = berita.kabupatenKODE
                        LIMIT $awalData , $dataPerPagination");


// select * from berita , kategoriberita where berita.kategoriberitaKODE = kategoriberita.kategoriberitaKODE

?>




<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">

            <!-- /# row -->
            <section id="main-content">


                <div class="row">
                    <div class="col-md-auto">
                        <div class="card">
                            <div class="card-body">
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                                    Insert Isi Berita
                                </button>


                                <!-- Modal -->
                                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Input Kabupaten</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">


                                                    <div class='form-group'>
                                                        <label for='beritaKODE'>Kode Berita</label>
                                                        <input type='text' class='form-control' id='beritaKODE' name='beritaKODE' placeholder='Kode Berita'>
                                                    </div>

                                                    <div class='form-group'>
                                                        <label for='beritaJUDUL'>Judul Berita</label>
                                                        <input type='text' class='form-control' id='beritaJUDUL' name='beritaJUDUL' placeholder='Judul Berita'>
                                                    </div>

                                                    <!-- <div class='form-group'>
                                                        <label for='kategoriberitKODE'>Kategori Berita</label>
                                                        <input type='text' class='form-control' id='kategoriberitKODE' name='kategoriberitKODE' placeholder='Kategori Berita'>
                                                    </div> -->

                                                    <div class='form-group'>
                                                        <label for='kategoriberita'>Kategori Berita</label>
                                                        <select name="kategoriberitaKODE" id="kategoriberitaKODE" class="form-control">

                                                            <?php foreach ($selectkategoriberita as $kategori) : ?>

                                                                <option value="<?= $kategori["kategoriberitaKODE"]; ?>">
                                                                    <?= $kategori["kategoriberitaNAMA"]; ?>
                                                                </option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>

                                                    <div class='form-group'>
                                                        <label for='eventKODE'>Event</label>
                                                        <input type='text' class='form-control' id='eventKODE' name='eventKODE' placeholder='Event'>
                                                    </div>

                                                    <div class='form-group'>
                                                        <label for='kabupatenKODE'>Nama Kabupaten</label>
                                                        <select name="kabupatenKODE" id="kabupatenKODE" class="form-control">

                                                            <?php foreach ($selectkabupaten as $kabupaten) : ?>

                                                                <option value="<?= $kabupaten["kabupatenKODE"]; ?>">
                                                                    <?= $kabupaten["kabupatenNAMA"]; ?>
                                                                </option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>

                                                    <div class='form-group'>
                                                        <label for='beritaISI'>Isi Berita</label>
                                                        <input type='text' class='form-control' id='beritaISI' name='beritaISI' placeholder='Isi Berita'>
                                                    </div>

                                                    <div class='form-group'>
                                                        <label for='beritaISI2'>Isi Berita 2</label>
                                                        <input type='text' class='form-control' id='beritaISI2' name='beritaISI2' placeholder='Isi Berita 2'>
                                                    </div>

                                                    <div class='form-group'>
                                                        <label for='beritaSUMBER'>Sumber Berita</label>
                                                        <input type='text' class='form-control' id='beritaSUMBER' name='beritaSUMBER' placeholder='Sumber Berita'>
                                                    </div>

                                                    <div class='form-group'>
                                                        <label for='beritaPENULIS'>Penulis Berita</label>
                                                        <input type='text' class='form-control' id='beritaPENULIS' name='beritaPENULIS' placeholder='Penulis Berita'>
                                                    </div>


                                                    <div class='form-group'>
                                                        <label for='tanggalberita'>Tanggal Berita</label>
                                                        <input type='text' class='form-control' id='datepicker' name='tanggalberita' placeholder='Tanggal Berita'>

                                                        <!-- <input type='text' class='form-control' id='datepicker' name='TanggalKecamatan' placeholder='TanggalKecamatan'> -->
                                                        <script></script>

                                                    </div>




                                                    <div class='form-group'>
                                                        <label for='Gambar'>Upload Gambar</label>
                                                        <input type='file' id='Gambar' name='Gambar'>
                                                    </div>

                                                    <div class='form-group'>
                                                        <button type='submit' name='submit' class='btn btn-primary'>Submit</button>
                                                    </div>

                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm">
                        <div class="card">
                            <div id="tablecontainer">
                                <table class="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th scope="col">No</th>
                                            <th scope="col">Kode Berita</th>
                                            <th scope="col">Judul Berita</th>
                                            <th scope="col">Kode Kategori Berita</th>
                                            <th scope="col">Kode Event</th>
                                            <th scope="col">Kode Kabupaten</th>
                                            <th scope="col">Isi Berita</th>
                                            <th scope="col">Isi Berita 2</th>
                                            <th scope="col">Sumber Berita</th>
                                            <th scope="col">Penulis Berita</th>
                                            <th scope="col">Tanggal Berita</th>
                                            <th scope="col">foto icon</th>
                                            <th scope="col">action</th>

                                        </tr>
                                    </thead>

                                    <tbody>

                                        <?php $x = $awalData + 1; ?>
                                        <?php foreach ($selectberita as $berita) : ?>
                                            <tr>

                                                <td><?= $x; ?></td>
                                                <td><?= $berita["beritaKODE"]; ?></td>
                                                <td><?= $berita["beritaJUDUL"]; ?></td>
                                                <td><?= $berita["kategoriberitaNAMA"]; ?></td>
                                                <td><?= $berita["eventKODE"]; ?></td>
                                                <td><?= $berita["kabupatenNAMA"]; ?></td>
                                                <td><?= $berita["beritaISI"]; ?></td>
                                                <td><?= $berita["beritaISI2"]; ?></td>
                                                <td><?= $berita["beritaSUMBER"]; ?></td>
                                                <td><?= $berita["beritaPENULIS"]; ?></td>
                                                <td><?= $berita["beritaTGL"]; ?></td>

                                                <td><img src="img/<?= $berita["beritaICONFOTO"]; ?>" alt="" width="50" height="50"> </td>
                                                <!-- <td><?= $berita["kabupatenKODE"]; ?></td> -->


                                                <td>
                                                    <a href="editisiberita.php?beritaKODE=<?= $berita["beritaKODE"]; ?>"><button type="submit" name="update" class="btn btn-secondary">Update</button></a> |
                                                    <a href="?beritaKODE=<?= $berita["beritaKODE"]; ?>"> <button type="submit" name="delete" class="btn btn-secondary">Delete</button></a>

                                                </td>



                                            </tr>
                                            <?php $x++; ?>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <!-- pagination -->
                                <nav aria-label="Page navigation example">
                                    <ul class="pagination">

                                        <?php if ($activePage > 1) : ?>
                                            <li class="page-item"><a class="page-link" href="?page=<?= $activePage - 1; ?>">Previous</a></li>
                                        <?php endif; ?>

                                        <?php for ($i = 1; $i <= $jumlahHalaman; $i++) : ?>

                                            <?php if ($i == $activePage) : ?>

                                                <li class="page-item"><a class="page-link" style="color:red" href="?page=<?= $i ?>"><?= $i; ?></a></li>
                                            <?php else : ?>
                                                <li class="page-item"><a class="page-link" href="?page=<?= $i ?>"><?= $i; ?></a></li>


                                            <?php endif; ?>
                                        <?php endfor; ?>

                                        <?php if ($activePage < $jumlahHalaman) : ?>
                                            <li class="page-item"><a class="page-link" href="?page=<?= $activePage + 1; ?>">Next</a></li>
                                        <?php endif; ?>
                                    </ul>
                                </nav>


                            </div>


                        </div>
                    </div>
                </div>



            </section>

        </div>
    </div>
</div>




<?php include "include/_footer.php"; ?>