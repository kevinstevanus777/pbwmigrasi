<?php


session_start();
require 'functions.php';
if (isset($_COOKIE['id']) && isset($_COOKIE['key'])) {

    $id = $_COOKIE['id'];
    $key = $_COOKIE['key'];

    $result = mysqli_query($connection, "SELECT USERNAME from user where id = $id");
    $row = mysqli_fetch_assoc($result);


    // cek cookie dan username
    if ($key === hash('sha256', $row['username'])) {
        $_SESSION['login'] = true;
    }


    // if ($_COOKIE['login'] == 'true') {
    //     $_SESSION['login'] = true;
    // }
}

if (isset($_SESSION["login"])) {
    header("Location: index.php");
    exit;
}







if (isset($_POST["login"])) {

    $username = $_POST["username"];

    $password = $_POST["password"];


    $result = mysqli_query($connection, "SELECT * FROM admin2 WHERE NAMAadmin = '$username'");


    // cek username
    if (mysqli_num_rows($result) === 1) {
        // cek password
        $row = mysqli_fetch_assoc($result);

        if (password_verify($password, $row["PASSWORDadmin"])) {

            // set session
            $_SESSION["login"] = true;

            $_SESSION["username"] = $username;

            if (isset($_POST['remember'])) {
                // buat cookie
                setcookie('id', $row['id'], time() + 60);
                setcookie('key', hash('sha256', $row['username']), time() + 60);
            }

            header("Location: index.php");
            exit;
        }
    }
    $error = true;
}

?>


<!-- new -->



<!-- new -->

<!DOCTYPE html>
<html lang="en">

<head>


    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="assets/forlogin/images/icons/favicon.ico" />
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/forlogin/vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/forlogin/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/forlogin/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/forlogin/vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/forlogin/vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/forlogin/vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/forlogin/vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/forlogin/vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="assets/forlogin/css/util.css">
    <link rel="stylesheet" type="text/css" href="assets/forlogin/css/main.css">
    <!--===============================================================================================-->
</head>

<body>
    <!-- <h1>login page</h1> -->


    <!-- <form action="" method="post">
        <ul>
            <li>


                <label for='username'>username</label>
                <input type='text' name='username' id='username'>

            </li>

            <li>


                <label for='password'>password</label>
                <input type='password' name='password' id='password'>
            </li>
            <div class='form-group'>
                <input type='checkbox' class='form-control' id='remember' name='remember' aria-describedby='emailHelp' placeholder='remember'>
                <label for='remember' class='col-sm-2 control-label'>remember me</label>

            </div>
            <li>
                <button type="submit" name="login">login</button>
            </li>




        </ul>

    </form> -->


    <!-- <form action="" method="post"> -->

    <div class="limiter">
        <div class="container-login100" style="background-image: url('assets/forlogin/images/bg-01.jpg');">
            <div class="wrap-login100 p-t-30 p-b-50">
                <span class="login100-form-title p-b-41">
                    Account Login
                </span>
                <form class="login100-form validate-form p-b-33 p-t-5" action="" method="post">

                    <div class="wrap-input100 validate-input" data-validate="Enter username">
                        <input class="input100" type="text" name="username" placeholder="User name">
                        <span class="focus-input100" data-placeholder="&#xe82a;"></span>
                    </div>

                    <div class="wrap-input100 validate-input" data-validate="Enter password">
                        <input class="input100" type="password" name="password" placeholder="Password">
                        <span class="focus-input100" data-placeholder="&#xe80f;"></span>
                    </div>

                    <?php if (isset($error)) : ?>

                        <p style="color:red; font-style: italic; text-align:center"> username/ password salah</p>
                    <?php endif; ?>
                    <div class="container-login100-form-btn m-t-32">
                        <button class="login100-form-btn" type="submit" name="login">
                            Login
                        </button>
                    </div>
                    <div class="container-login100-form-btn m-t-32">

                        <a class="login100-form-btn" href="registrasi.php" role="button">
                            Register
                        </a>

                    </div>



                </form>
            </div>
        </div>
    </div>


    <!-- </form> -->

    <div id="dropDownSelect1"></div>

    <!--===============================================================================================-->
    <script src="vendor/jquery/jquery-3.2.1.min.js"></script>
    <!--===============================================================================================-->
    <script src="vendor/animsition/js/animsition.min.js"></script>
    <!--===============================================================================================-->
    <script src="vendor/bootstrap/js/popper.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <!--===============================================================================================-->
    <script src="vendor/select2/select2.min.js"></script>
    <!--===============================================================================================-->
    <script src="vendor/daterangepicker/moment.min.js"></script>
    <script src="vendor/daterangepicker/daterangepicker.js"></script>
    <!--===============================================================================================-->
    <script src="vendor/countdowntime/countdowntime.js"></script>
    <!--===============================================================================================-->
    <script src="js/main.js"></script>
</body>

</html>