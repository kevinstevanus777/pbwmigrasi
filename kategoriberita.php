<?php
include "include/_header.php";





if (isset($_POST["submit"])) {
    if (insertkategoriberita($_POST) > 0) {
        echo "<script>
        alert('data berhasil ditambahkan!');
        document.location.href= ''
        </script>";
    } else {
        echo "<script>
        alert('data berhasil ditambahkan!');
        document.location.href= ''
        </script>";
    }
}

if (isset($_GET["kategoriberitaKODE"])) {


    if (deletedata($_GET["kategoriberitaKODE"], "kategoriberita", "kategoriberitaKODE") > 0) {
        echo "<script>
    alert('data berhasil didelete!');
    document.location.href = 'kategoriberita.php'
</script>";
    } else {
        echo "<script>
        alert('data tidak berhasil didelete!');
        document.location.href = 'kategoriberita.php'
    </script>";
    }
}


$dataPerPagination = 5;
$jumlahTotalIndex = count(query("SELECT * FROM kategoriberita"));
$jumlahHalaman = ceil($jumlahTotalIndex / $dataPerPagination);
$activePage = (isset($_GET["page"])) ? $_GET["page"] : 1;
$awalData = ($dataPerPagination * $activePage) - $dataPerPagination;
$selectkategoriberita = query("SELECT * FROM kategoriberita LIMIT $awalData , $dataPerPagination");

// $selectdestinasiwisata = query("SELECT * FROM obyekwisata, kategoriwisata,kecamatan
// WHERE obyekwisata.kategoriKODE = kategoriwisata.kategoriKODE and obyekwisata.kecamatanKODE = kecamatan.kecamatanKODE LIMIT $awalData , $dataPerPagination");


?>



<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">

            <!-- /# row -->
            <section id="main-content">


                <div class="row">
                    <div class="col-md-auto">
                        <div class="card">
                            <div class="card-body">
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                                    Insert Kategori Berita
                                </button>


                                <!-- Modal -->
                                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Input Kabupaten</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form class="form-horizontal" action="" method="post">


                                                    <div class='form-group'>
                                                        <label for='kategoriberitaKODE'>Kode Kategori Berita</label>
                                                        <input type='text' class='form-control' id='kategoriberitaKODE' name='kategoriberitaKODE' placeholder='Kode Kategori Berita'>
                                                    </div>

                                                    <div class='form-group'>
                                                        <label for='kategoriberitaNAMA'>Nama Kategori Berita</label>
                                                        <input type='text' class='form-control' id='kategoriberitaNAMA' name='kategoriberitaNAMA' placeholder='Nama Kategori Berita'>
                                                    </div>

                                                    <div class='form-group'>
                                                        <label for='kategoriberitaKET'>Keterangan kategori berita</label>
                                                        <input type='text' class='form-control' id='kategoriberitaKET' name='kategoriberitaKET' placeholder='Keterangan kategori berita'>
                                                    </div>


                                                    <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>


                <div class="row">
                    <div class="col-sm">
                        <div class="card">

                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Kode Kategori</th>
                                        <th scope="col">Nama Kategori</th>
                                        <th scope="col" style="width: 30%;">Keterangan Kategori</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>

                                <tbody>

                                    <?php $x = 1; ?>
                                    <?php foreach ($selectkategoriberita as $data) : ?>
                                        <tr>

                                            <td><?= $x; ?></td>
                                            <td><?= $data["kategoriberitaKODE"]; ?></td>
                                            <td><?= $data["kategoriberitaNAMA"]; ?></td>
                                            <td><?= $data["kategoriberitaKET"]; ?></td>
                                            <td>
                                                <a href="editkategoriberita.php?kategoriberitaKODE=<?= $data["kategoriberitaKODE"]; ?>"> <button type="submit" name="update" class="btn btn-secondary">Update</button></a>
                                                <a href="?kategoriberitaKODE=<?= $data["kategoriberitaKODE"]; ?>"> <button type="submit" name="delete" class="btn btn-secondary">Delete</button></a>
                                            </td>

                                        </tr>
                                        <?php $x++; ?>
                                    <?php endforeach; ?>

                                </tbody>
                            </table>

                            <!-- pagination -->
                            <nav aria-label="Page navigation example">
                                <ul class="pagination">

                                    <?php if ($activePage > 1) : ?>
                                        <li class="page-item"><a class="page-link" href="?page=<?= $activePage - 1; ?>">Previous</a></li>
                                    <?php endif; ?>

                                    <?php for ($i = 1; $i <= $jumlahHalaman; $i++) : ?>

                                        <?php if ($i == $activePage) : ?>

                                            <li class="page-item"><a class="page-link" style="color:red" href="?page=<?= $i ?>"><?= $i; ?></a></li>
                                        <?php else : ?>
                                            <li class="page-item"><a class="page-link" href="?page=<?= $i ?>"><?= $i; ?></a></li>


                                        <?php endif; ?>
                                    <?php endfor; ?>

                                    <?php if ($activePage < $jumlahHalaman) : ?>
                                        <li class="page-item"><a class="page-link" href="?page=<?= $activePage + 1; ?>">Next</a></li>
                                    <?php endif; ?>
                                </ul>
                            </nav>

                        </div>
                    </div>
                </div>
            </section>

        </div>
    </div>
</div>




<?php include "include/_footer.php"; ?>