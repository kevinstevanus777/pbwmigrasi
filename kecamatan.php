<?php
include "include/_header.php";

// pagination

$dataPerPagination = 5;
// $querytotaldata = mysqli_query($connection, "SELECT * FROM kecamatan");
$jumlahTotalIndex = count(query("SELECT * FROM kecamatan"));
$jumlahHalaman = ceil($jumlahTotalIndex / $dataPerPagination);


$activePage = (isset($_GET["page"])) ? $_GET["page"] : 1;

$awalData = ($dataPerPagination * $activePage) - $dataPerPagination;

$selectkecamatan = query("SELECT * FROM kecamatan LIMIT $awalData , $dataPerPagination");


// $selectkecamatan = query("SELECT * FROM kecamatan");
$selectkabupaten = query("SELECT * FROM kabupaten");

if (isset($_POST["submit"])) {



    if (insertkecamatan($_POST) > 0) {
        echo "<script>
    alert('data berhasil ditambahkan!');
    document.location.href = ''
</script>";
    } else {
        echo "<script>
    alert('data tidak berhasil ditambahkan!');
    document.location.href = ''
</script>";
    }
}



if (isset($_GET["kecamatanKODE"])) {


    if (deletedata($_GET["kecamatanKODE"], "kecamatan", "kecamatanKODE") > 0) {
        echo "<script>
    alert('data berhasil didelete!');
    document.location.href = 'kecamatan.php'
</script>";
    } else {
        echo "<script>
        alert('data tidak berhasil didelete!');
        document.location.href = 'kecamatan.php'
    </script>";
    }
}

?>

<!-- content -->
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">


            <!-- /# row -->
            <section id="main-content">


                <div class="row">
                    <div class="col-md-auto">
                        <div class="card">
                            <div class="card-body">
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                                    Insert Kecamatan
                                </button>


                                <!-- Modal -->
                                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Input Kecamatan</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">


                                                    <div class='form-group'>
                                                        <label for='KodeKecamatan'>KodeKecamatan</label>
                                                        <input type='text' class='form-control' id='KodeKecamatan' name='KodeKecamatan' placeholder='KodeKecamatan'>
                                                    </div>

                                                    <div class='form-group'>
                                                        <label for='NamaKecamatan'>NamaKecamatan</label>
                                                        <input type='text' class='form-control' id='NamaKecamatan' name='NamaKecamatan' placeholder='NamaKecamatan'>
                                                    </div>

                                                    <div class='form-group'>
                                                        <label for='AlamatKecamatan'>AlamatKecamatan</label>
                                                        <input type='text' class='form-control' id='AlamatKecamatan' name='AlamatKecamatan' placeholder='AlamatKecamatan'>
                                                    </div>


                                                    <div class="form-group">
                                                        <label for="KeteranganKecamatan">Keterangan Kecamatan</label>
                                                        <textarea class="form-control" id="KeteranganKecamatan" name="KeteranganKecamatan" placeholder="Keterangan" rows="6"></textarea>
                                                    </div>

                                                    <div class='form-group'>
                                                        <label for='datepicker'>TanggalKecamatan</label>

                                                        <input type='text' class='form-control' id='datepicker' name='TanggalKecamatan' placeholder='TanggalKecamatan'>
                                                        <script></script>

                                                    </div>



                                                    <div class='form-group'>
                                                        <label for='NamaKabupaten'>NamaKabupaten</label>
                                                        <select name="NamaKabupaten" id="NamaKabupaten" class="form-control">

                                                            <?php foreach ($selectkabupaten as $kabupaten) : ?>

                                                                <option value="<?= $kabupaten["kabupatenKODE"]; ?>">
                                                                    <?= $kabupaten["kabupatenNAMA"]; ?>
                                                                </option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>

                                                    <div class='form-group'>
                                                        <label for='Gambar'>Upload Gambar</label>
                                                        <input type='file' id='Gambar' name='Gambar'>
                                                    </div>


                                                    <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm">
                        <div class="card">
                            <div id="tablecontainer">
                                <table class="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th scope="col">No</th>
                                            <th scope="col">Kode Kecamatan</th>
                                            <th scope="col">Nama Kecamatan</th>
                                            <th scope="col" style="width:10%">Alamat Kecamatan</th>
                                            <th scope="col" style="width:25%">Keterangan Kecamatan</th>
                                            <th scope="col">Tanggal Kecamatan</th>
                                            <th scope="col">Foto Kecamatan</th>
                                            <th scope="col">Kode Kabupaten</th>
                                            <th scope="col" width="16%">Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>

                                        <?php $x = $awalData + 1; ?>
                                        <?php foreach ($selectkecamatan as $kecamatan) : ?>
                                            <tr>

                                                <td><?= $x; ?></td>
                                                <td><?= $kecamatan["kecamatanKODE"]; ?></td>
                                                <td><?= $kecamatan["kecamatanNAMA"]; ?></td>
                                                <td><?= $kecamatan["kecamatanALAMAT"]; ?></td>
                                                <td><?= $kecamatan["kecamatanKET"]; ?></td>
                                                <td><?= $kecamatan["kecamatanTGL"]; ?></td>

                                                <td><img src="img/<?= $kecamatan["kecamatanFOTO"]; ?>" alt="" width="50" height="50"> </td>
                                                <td><?= $kecamatan["kabupatenKODE"]; ?></td>


                                                <td>
                                                    <a href="editkecamatan.php?kecamatanKODE=<?= $kecamatan["kecamatanKODE"]; ?>"><button type="submit" name="update" class="btn btn-secondary">Update</button></a> |
                                                    <a href="?kecamatanKODE=<?= $kecamatan["kecamatanKODE"]; ?>"> <button type="submit" name="delete" class="btn btn-secondary">Delete</button></a>


                                                </td>



                                            </tr>
                                            <?php $x++; ?>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <!-- pagination -->
                                <nav aria-label="Page navigation example">
                                    <ul class="pagination">

                                        <?php if ($activePage > 1) : ?>
                                            <li class="page-item"><a class="page-link" href="?page=<?= $activePage - 1; ?>">Previous</a></li>
                                        <?php endif; ?>

                                        <?php for ($i = 1; $i <= $jumlahHalaman; $i++) : ?>

                                            <?php if ($i == $activePage) : ?>

                                                <li class="page-item"><a class="page-link" style="color:red" href="?page=<?= $i ?>"><?= $i; ?></a></li>
                                            <?php else : ?>
                                                <li class="page-item"><a class="page-link" href="?page=<?= $i ?>"><?= $i; ?></a></li>


                                            <?php endif; ?>
                                        <?php endfor; ?>

                                        <?php if ($activePage < $jumlahHalaman) : ?>
                                            <li class="page-item"><a class="page-link" href="?page=<?= $activePage + 1; ?>">Next</a></li>
                                        <?php endif; ?>
                                    </ul>
                                </nav>


                            </div>


                        </div>
                    </div>
                </div>


            </section>
        </div>
    </div>
</div>

<?php include "include/_footer.php"; ?>